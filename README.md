![wkx.png](https://codeberg.org/johndovern/wkx/raw/branch/master/wkx.png)

# wkx - Which-key via X.

wkx gives user's which-key like visual feedback while executing key
chords. It can also be used as a stand alone hotkey daemon if you like.

# Screenshot

![example.png](https://codeberg.org/johndovern/wkx/raw/branch/master/example.png)

# Usage

``` example
WKX(1)                       General Commands Manual                      WKX(1)

NAME
       wkx - Which-key via X

SYNOPSIS
       wkx  [-h]  [-v]  [-d]  [-D]  [-w] [-W] [-b BINDS_FILE] [-c CONF_FILE] [-k
       KEYS_FILE] [-o OUTPUT_FILE] [-p KEY(s)] [-r REDIR_FILE]

DESCRIPTION
       wkx provides visual feedback for executing user defined key  chords.   It
       may also be used as it's own hotkey daemon if you like.

OPTIONS
       -h, --help
              Print help message and exit.

       -v, --version
              Print version and exit.

       -d, --daemon
              Run wkx as a hotkey daemon.

       -D, --debug
              Print debugging information during execution.

       -w, --write-binds
              Parse  BINDS_FILE and write a properly formatted OUTPUT_FILE file.
              If BINDS_FILE is not given then $XDG_CONFIG_HOME/wkx/bindsrc  will
              be     used.      If     OUTPUT_FILE    is    not    given    then
              $WKX_SRC_HOME/conf/binds.h is the file written to.   See  ENVIRON‐
              MENT for more info.

       -W, --write-keys
              Parse  KEYS_FILE  and write a properly formatted OUTPUT_FILE file.
              If KEYS_FILE is not given then $XDG_CONFIG_HOME/wkx/keysrc will be
              used.   If OUTPUT_FILE is not given then $WKX_SRC_HOME/conf/keys.h
              is the file written to.  See ENVIRONMENT for more info.

       -b, --binds-file BINDS_FILE
              If given without --write-binds then wkx will  use  this  file  for
              it's normal operation instead of $XDG_CONFIG_HOME/wkx/bindsrc.

       -c, --conf-file CONF_FILE
              wkx  will  use  this  file  for  it's  normal operation instead of
              $XDG_CONFIG_HOME/wkx/wkxrc.

       -k, --keys-file KEYS_FILE
              If given without --write-keys then wkx will use this file for it's
              normal operation instead of $XDG_CONFIG_HOME/wkx/keysrc.

       -o, --output OUTPUT_FILE
              Write output from --write-binds or --write-keys to the given file.

       -p, --press-keys KEYS(s)
              List any keys here that you want pre-pressed.  NOTE the keys given
              should be space seperated.

       -r, --redir-file REDIR_FILE
              Direct a commands output to the given file instead of STDOUT.   If
              the   path   given   is   relative,  then  assumed  path  is  $TM‐
              PDIR/wkx/REDIR_FILE.  If $TMPDIR is not set it is  assumed  to  be
              /tmp.

ENVIRONMENT
       Feel free to download and edit the wkx source code to your liking.

       WKX_SRC_HOME
              You  can  set  this  environment  variable  so  that  options like
              --write-binds and --write-keys know where to output the  necessary
              source code.

FILES
       bindsrc
              This file is located in $XDG_CONFIG_HOME/wkx but you can specify a
              differnt file with the --binds-file flag.   The  syntax  for  this
              file  is  simple,  and also configurable.  See bindsrc(1) for more
              information.

       keysrc This file is located in $XDG_CONFIG_HOME/wkx but you can specify a
              differnt file with the --keys-file flag.  The syntax for this file
              is simple, and also configurable.  See keysrc(1) for more informa‐
              tion.

       wkxrc  This file is located in $XDG_CONFIG_HOME/wkx but you can specify a
              differnt file with the --conf-file flag.  See  wkxrc(1)  for  more
              information.

SIGNALS
       If  wkx is being used as a hotkey daemon it will respond to the following
       signals.

       SIGURS1
              This signal will reload wkx's keysrc file, or the  file  given  at
              runtime.

       SIGURS2
              This signal will toggle the grabbing state of wkx.

                                                                          WKX(1)
```

## Files

See the suckless branch for info on the `binds.h`, `config.h`, and
`keys.h` files.

### bindsrc

``` example
BINDSRC(1)                   General Commands Manual                  BINDSRC(1)

DESCRIPTION
       This man page describes the syntax of a bindsrc file as understood by
       wkx(1)

SYNTAX
       The syntax of the bindsrc file is simple but does need some explanation.
       Here is an example of a bindsrc file:

           # Browsers prefix
           b Browsers
               # Profiles prefix
               :F Firefox profiles
                   ::f Focus == firefox -P Focus
                   ::l Learning == firefox -P Learning
                   ::e Entertainment == firefox -P Entertainment
               # Sites prefix
               :s Sites
                   ::m My site == firefox "https://www.my-site.org"
                   ::o Other site == firefox "https://www.my-other-site.org"
               :f Firefox == firefox
           # emacs prefix
           TAB Emacs
               :e Open emacs == emacs
               # Projects prefix
               :C-p Projects
                   ::M-m My project == emacs "~/Projects/My Project"
                   ::o Other project == emacs "~/Projects/My Other Project"
           # mpv prefix
           m テープリワインダー
               :m My music == mpv "~/Music/my_playlist.m3u"
               :b Empty mpv    ==
               :e Multi line mpv \
               == mpv \
                  --flag value \
                  "/path/to/file"

       And here is the anatomy of any given bindsrc line:

              [LEVEL_CHAR] [KEY] [DESCRIPTION] [COMMAND_SEP] [COMMAND]

       The only non-optional item from the above list is the KEY, aside from
       that you can ommit all other options.  Let's first look at comments.

       COMMENTS
              The default comment character is '#'.  See wkxrc(1) for info on
              how to change this.  The following are all lines that are
              considerd comments:

                  # comment one
                      # comment two

              As you can see leading white space is not taken into
              consideration.

       LEVEL_CHAR
              The default level char is ':'.  See wkxrc(1) for info on how to
              change this.  The purpose of this character is to enable the
              nesting of chords.  Let's
              look at one of the above lines to see the significance of this
              character:

                  # Browsers prefix
                  b Browsers
                      # Profiles prefix
                      :F Firefox profiles
                          ::f Focus == firefox -P Focus

              With this short example you can see that if you wanted to run the
              command 'firefox -P Focus' you would need to run wkx and press 'b'
              and  then  press 'F' before you could finally press 'f' to have
              the command run.  This will, hopefully, make more sense as you
              read on.

       KEY    As mentioned before the KEY portion of a line is not optional.
              There are three types of keys:

                     UNMODED KEYS, MODED KEYS, and SPECIAL KEYS.

              UNMODED KEYS

              An  unmodified  key  is one that is can be typed with or without
              the shift key. These are NOT keysyms. If you want a chord to use
              '=' then put '=' and not 'equal' as you would in your keysrc file.

              MODED KEYS

              Moded keys are keys that are pressed in combination with one of
              the following modifiers:

                     Ctrl, Alt, and Hyper.

              To add a moded key to your bindsrc file you would add something
              like 'C-M-H-k'.  This would add a chord that triggers when 'k' is
              pressed  along  with all three of the above modifiers.

              The  order  of the mods is quite important.  Ctrl has the highest
              priority followed by Alt and finally Hyper.  If one or more of
              these modifiers is to be present in a key then this order of
              priority must be followed.

                     To add Ctrl to a key simply add 'C-' to the key.

                     To add Alt to a key simply add 'M-' to the key.

                     To add Hyper to a key simply add 'H-' to the key.

              These mod rules apply to all keys.

              SPECIAL KEYS

              The following keys are considerd special keys:

              LEFT |  RIGHT   |  UP        |  DOWN   |
              TAB  |  SPACE   |  RETURN    |  DELETE |  ESCAPE
              HOME |  PAGE_UP |  PAGE_DOWN |  END    |  BEGIN

              To add them to your bindsrc use the these respective forms:

              Left |  Right   |  Up        |  Down   |
              TAB  |  SPC     |  RET       |  DEL    |  ESC
              Home |  PgUp    |  PgDown    |  End    |  Begin

              Apply 'S-' as a prefix to any of the above to specify the shifted
              version of the key.  This is unique to these special keys, do not
              do  this  for  any other keys.

       DESCRIPTION
              The description can be whatever you like. However, you MUST have
              some white space seperating your KEY and your DESCRIPTION.

       COMMAND_SEP
              The  default command seperator is the '=' character.  See wkxrc(1)
              for info on how to change this.  Whatever character you use, you
              must use 2 of them back to back to indicate the end of your KEY /
              DESCRIPTION, and the begining of your COMMAND.

       COMMAND
              If it works in a terminal it should work here.  You can end a line
              with a backslash, '\', if you need more than one line for your
              command.

CONCLUSION
       That pretty much sums up the syntax of the bindsrc file.  If you have any
       comments, questions, or improvements please reachout at the projects
       codebreg page:

              https://codebreg.org/johndovern/wkx
```

### wkxrc

``` example
WKXRC(1)                     General Commands Manual                    WKXRC(1)

DESCRIPTION
       This  man  page  describes  the  configuration  options available for the
       wkx(1) program.  These configuration options are also applicable  to  the
       config[.def].h  file for wkx.  To set a variable in the wkxrc file simply
       use the following syntax:

              variable = value

       Easy right?  You may suround a value with single or double quotes if  you
       like.   If  you are setting a value to a single or double qoute it is ad‐
       vised that you suround it with the opposite character.

OPTIONS
       command_char
              The command_char variable is  a  single  character.   The  default
              value for this character is '='.  The purpose of this character is
              to separate a command from whatever may come before it.  See bind‐
              src(1) and keysrc(1) for more info.

       comment_char
              The  comment_char  variable is a single character used to indicate
              comments on a given line.  The default value for this character is
              '#'.  See bindsrc(1) and keysrc(1) for more info.

       level_char
              The  level_char variable is used to denote a lines level as under‐
              stood in a bindsrc(1) file.  The default value for this  character
              is ':'.

       delim  The delim variable is a sting of 0 or more characters.  This vari‐
              able is used to separate a KEY from a DESCRIPTION when wkx is dis‐
              playing  the  available  key chords.  A space is added to the left
              and right of this variable regardless of what is set.  The default
              value for this variable is '->'.

       parse_conf
              The parse_conf variable is an unsigned integer.  Set this variable
              to 1 to enable wkxrc parsing.  Set it to 0 to disable config pars‐
              ing.   If this variable is set to 0 passing a config file with the
              --conf-file option will enable parsing regardless  of  this  vari‐
              able's setting.

       parse_binds
              The  parse_binds  variable is an unsigned integer.  Set this vari‐
              able to 1 to enable bindsrc parsing.  Set it to 0 to disable pars‐
              ing.  If this variable is set to 0 passing a bindsrc file with the
              --binds-file option will enable parsing regardless of  this  vari‐
              able's setting.

       parse_keys
              The parse_keys variable is an unsigned integer.  Set this variable
              to 1 to enable keysrc parsing.  Set it to 0  to  disable  parsing.
              If  this  variable  is  set  to  0  passing a keysrc file with the
              --keys-file option will enable parsing regardless  of  this  vari‐
              able's setting.

       cols   The  cols variable is an unsigned integer.  This number represents
              the MAXIMUM number of columns that wkx should display.  This  num‐
              ber will never be exceded.  If the number of key chords to display
              excedes the max number of columns then wkx will do  it's  best  to
              divide  these  into the number of columns that would result in the
              fewest lines being drawn.

       wkx_x  The wkx_x variable is a signed integer.  Use this variable to  po‐
              sition  the  left  edge wkx at the desired 'x' coordinate.  If you
              don't want to think about this then set this value to '-1'.   When
              set  to  '-1' wkx will position itself along the 'x' axis at 1/4th
              of your monitor's width.

       wkx_y  The wkx_y variable is a signed integer.  Use this variable to  po‐
              sition  the bottom edge of wkx at the desired 'y' coordinate.  The
              value is relative to the bottom of your monitor and  not  the  top
              unlike  most other X11 programs.  If you don't want to think about
              this then set this value to '-1'.  When set to '-1' wkx will posi‐
              tion  its  bottom  edge along the 'y' axis at 1/10th of your moni‐
              tor's height.

       wkx_w  The wkx_w variable is a signed integer.   This  variable  dictates
              how  wide  wkx  should  be.  If you don't want to think about this
              then set this value to '-1' or '0'.  When set  to  '-1'  wkx  will
              make  itself  half  as  wide as your monitor.  When set to '0' wkx
              will make itself as wide as your monitor.  All  other  values  are
              fully respected.

       border_w
              The  border_w variable is an unsigned integer.  This variable dic‐
              tates the border width of wkx.

       col_[fg|bg|bd]
              These variables set the foreground, background, and border  colors
              of  wkx  respectively.   They are strings and their default values
              are '#F8F7FB', '#21242B', and '#51AFEF' respectively.

       def_redir
              The def_redir variable is a string representing the location of  a
              file.   This file will be used by wkx to redirect a command's out‐
              put while it is running as a daemon, or during regular  operation.
              If the path is relative then the file is assumed to exist in '$TM‐
              PDIR/wkx'.  If the $TMPDIR environment variable is  not  set  then
              '/tmp'  is  the  assumed  location.  Set the value to 'NULL' or an
              empty string if you do not want to redirect a command's output.

       shell  The shell variable is a string that is an  absolute  path  to  the
              shell  you  wish  to  use  for  command  execution.   It is set to
              '/bin/sh' by default.

       font   The font variable takes a string  representing  a  font  that  you
              would  like wkx to use for it's text.  The default value is 'mono‐
              space:size=10'.  This variable may be defined multiple  times  and
              you may set up to 256 fonts.  The order of the fonts is important.
              Set your most desired font first  and  any  backup  fonts  (emoji,
              foriegn character fonts, etc.) afterwards.

                                                                        WKXRC(1)
```

### keysrc

``` example
KEYSRC(1)                    General Commands Manual                   KEYSRC(1)

DESCRIPTION
       This  man  page  describes  the  syntax of a keysrc file as understood by
       wkx(1)

SYNTAX
       The syntax of the keysrc file is simple but does need  some  explanation.
       Here is an example of a keysrc file:

           # comment
           ctrl + super + a == notify-send "wkx" "a"
           alt + period == notify-send "wkx" "period" \
                           ; notify-send "wkx" "period 2"

       And here is the anatomy of any given bindsrc line:

              [MODIFIERS] [KEYSYM] [COMMAND_SEP] [COMMAND]

       Let's first look at comments.

       COMMENTS
              The  default  comment  character is '#'.  See wkxrc(1) for info on
              how to change this.  The following are all lines that are  consid‐
              erd comments:

                  # comment one
                      # comment two

              As  you  can  see leading white space is not taken into considera‐
              tion.

       MODIFIERS
              The following are all valid modifier aliases:

                     alt, ctrl, meta, mod1, mod2, mod3, mod4, mod5,  shift,  su‐
                     per, hyper, and control.

       KEY    Take         a        look        at        https://cgit.freedesk‐
              top.org/xorg/proto/x11proto/tree/keysymdef.h for an explicit  list
              of  all  keysyms  you  may use.  Any key found there may be put in
              your keysrc file without the 'XK_' prefix.  To use XF86  keys  you
              would do something like this

                  super + XF86MonBrightnessUp == xbacklight -inc 5

              See                                         https://cgit.freedesk‐
              top.org/xorg/proto/x11proto/tree/XF86keysym.h for a list of avail‐
              able keys.  Simply remove the 'XK_' from the desired key.

       COMMAND_SEP
              The  default command seperator is the '=' character.  See wkxrc(1)
              for info on how to change this.  Whatever character you  use,  you
              must  use  2  of them back to back to indicate the the begining of
              your COMMAND.

       COMMAND
              If it works in a terminal it should work here.  You can end a line
              with  a  backslash,  '\',  if you need more than one line for your
              command.

CONCLUSION
       That pretty much sums up the syntax of the keysrc file.  If you have  any
       comments,  questions,  or  improvements  please  reachout at the projects
       codebreg page:

              https://codebreg.org/johndovern/wkx

                                                                       KEYSRC(1)
```

# Building

wkx requires the following `Xlib`. `Xinerama` is an optional dependency.

``` bash
make && sudo make install
```

# Credit

This project is very much in debt to those behind
[dmenu](https://tools.suckless.org/dmenu/),
[sxhkd](https://github.com/baskerville/sxhkd), and the suckless team in
general.
