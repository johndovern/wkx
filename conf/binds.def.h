/* See LICENSE file for copyright and license details. */
#ifndef WKX_BINDS_H_
#define WKX_BINDS_H_

#define PREFIX(...) (Chord[]){ __VA_ARGS__, { NULL, NULL, NULL, NULL } }
#define CHORDS(...) { __VA_ARGS__, { NULL, NULL, NULL, NULL } }

const Chord chords[] = CHORDS(
    /* bind     desc            command             prefix */
    { "C-a",      "A keybind",    "echo \"C-a\"",      NULL },
    { "M-k",      "K prefix",     NULL,               PREFIX(
        { "i",      "I keybind",    "echo \"i\"",    NULL },
        { "l",      "L keybind",    "echo \"l\"",    NULL },
        { "t",      "T prefix",     NULL,               PREFIX(
            { "w",      "w Keybind",    "echo \"w\"", NULL}
        )},
        { "TAB",      "TAB keybind",    "echo \"TAB\"",      NULL }
    )}
);

#endif /* WKX_BINDS_H_ */
