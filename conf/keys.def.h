/* See LICENSE file for copyright and license details. */
#ifndef WKX_KEYS_H
#define WKX_KEYS_H

#include <X11/XF86keysym.h>

const Key keys[] = {
    /* mod(s)           key             cmd */
    { Mod1Mask,         XK_comma,       "notify-send 'test'" },
};

#endif /* WKX_KEYS_H */
