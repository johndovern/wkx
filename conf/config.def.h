/* See LICENSE file for copyright and license details. */
#ifndef WKX_CONFIG_H_
#define WKX_CONFIG_H_

/* command sep for bindsrc parsing */
char command_char        = '=';
/* comment char for bindsrc parsing */
char comment_char        = '#';
/* level char for bindsrc parsing */
char level_char          = ':';
/* Seperator when displaying binds */
char *delim              = "->";
/* 0 disables parsing without -c flag */
unsigned int parse_conf  =  1;
/* 0 disables parsing without -b || -w flag */
unsigned int parse_binds =  1;
/* 0 disables parsing without -K || -W flag */
unsigned int parse_keys  =  1;
/* Max number of columns to use */
unsigned int cols        =  4;
/* put wkx at this x offset */
int wkx_x                = -1;
/* put wkx at this y offset from bottom */
int wkx_y                = -1;
/* make wkx this wide */
int wkx_w                = -1;
/* wkx border width */
unsigned int border_w    =  2;

/* Foreground, background, and border colors respectively */
char *col_fg = "#F8F7FB";
char *col_bg = "#21242B";
char *col_bd = "#51AFEF";

/* Default redirection file. Set to NULL for none. Relative paths are
 * resolved to /tmp/wkx/${def_redir} */
char *def_redir = NULL;

/* Default redirection file when running as a hotkey daemon.
 * Set to NULL for none. Relative paths are
 * resolved to /tmp/wkx/${def_redir} */
char *daemon_redir = NULL;

/* Default shell */
char *shell = "/bin/sh";

const char *fonts[] = {
    "monospace:size=10"
};

#endif /* WKX_CONFIG_H_ */
