# wkx - Which-key via X
# See LICENSE file for copyright and license details.

# BIN
OUT   = wkx

# Dirs
OBJD  = ./obj
CONFD = ./conf
MAND  = ${DESTDIR}${MANPREFIX}/man1

# PATH
VPATH = . ./src ./inc ${OBJD} ${CONFD}

# Files
CONF  = ${notdir ${wildcard ${CONFD}/*.def.h}}
DEP   = ${SRC:.c=.d}
MAN   = ${wildcard ./man/*.1}
OBJ   = ${SRC:.c=.o}
SRC   = ${notdir ${foreach D,${VPATH},${wildcard ${D}/*.c}}}

include config.mk

all: options ${OUT}

options:
	@echo ${OUT} build options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"
	@echo "CONF     = ${CONF}"
	@echo "DEP      = ${DEP}"
	@echo "OBJ      = ${OBJ}"
	@echo "SRC      = ${SRC}"

%.o:%.c
	@mkdir -p ${OBJD}
	${CC} -c ${CFLAGS} -o ${OBJD}/$@ $<

${OBJ}: ${CONF:.def.h=.h} config.mk

%.h:
	cp ${CONFD}/${@:.h=.def.h} ${CONFD}/$@

${OUT}: ${OBJ}
	${CC} -o $@ ${patsubst %,${OBJD}/%,${OBJ}} ${LDFLAGS}

clean:
	rm -f ${OUT} ${patsubst %,${OBJD}/%,${OBJ}} \
	${patsubst %,${OBJD}/%,${DEP}} ${OUT}-${VERSION}.tar.gz
	rm -f ${foreach F,${CONF:.def.h=.h},${CONFD}/${F}}

dist: clean
	mkdir -p ${OUT}-${VERSION}/src \
		${OUT}-${VERSION}/inc \
		${OUT}-${VERSION}/conf
	cp -R ${CONFD}/binds.def.h \
		${CONFD}/config.def.h \
		${CONFD}/keys.def.h \
		${OUT}-${VERSION}/conf
	cp -R ./src ${OUT}-${VERSION}
	cp -R ./inc ${OUT}-${VERSION}
	cp -R ./man ${OUT}-${VERSION}
	cp -R LICENSE Makefile README.md config.mk \
		${OUT}.png ${OUT}-${VERSION}
	tar -cf ${OUT}-${VERSION}.tar ${OUT}-${VERSION}
	gzip ${OUT}-${VERSION}.tar
	rm -rf ${OUT}-${VERSION}

install: all
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f ${OUT} ${DESTDIR}${PREFIX}/bin
	chmod 755 ${DESTDIR}${PREFIX}/bin/${OUT}
	mkdir -p ${DESTDIR}${MANPREFIX}/man1
	cp -R ${MAN} ${MAND}
	chmod 644 ${patsubst %,${MAND}/%,${notdir ${MAN}}}

uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/${OUT}\
		${patsubst %,${MAND}/%,${notdir ${MAN}}}

.PHONY: all options clean dist install uninstall
