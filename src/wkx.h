/* See LICENSE file for copyright and license details. */
#ifndef WKX_H_
#define WKX_H_

#include <linux/limits.h>

#include "helpers.h"
#include "types.h"
#include "xwin.h"

/* envs */
#define CONFIG_HOME_ENV     "XDG_CONFIG_HOME"
#define TMP_ENV             "TMPDIR"
#define TMP_DIR             "/tmp"
#define BINDS_PATH          "wkx/bindsrc"
#define CONFIG_PATH         "wkx/wkxrc"
#define KEYS_PATH           "wkx/keysrc"
#define FILE_SIZE           PATH_MAX
#define CWD_SIZE            PATH_MAX - MAXLEN

/* Bitmasks */
#define DEBUG_FLAG          (1L<<0)
#define DELIM_FLAG          (1L<<1)
#define FONTS_FLAG          (1L<<2)
#define CHORD_FLAG          (1L<<3)
#define KLIST_FLAG          (1L<<4)
#define KARAY_FLAG          (1L<<5)
#define COLFG_FLAG          (1L<<6)
#define COLBG_FLAG          (1L<<7)
#define COLBD_FLAG          (1L<<8)
#define REDIR_FLAG          (1L<<9)
#define MULTI_FLAG          (1L<<10)

/* Files/paths */
extern char binds_file[];
extern char *binds_path;
extern char config_file[];
extern char *config_path;
extern char keys_file[];
extern char *keys_path;
extern char redir_file[];
extern char *redir_path;
extern char output_file[];
extern char *output_path;
extern char cwd[];

/* lists/arrays */
extern Bind *binds_head, *binds_tail;
extern Chord *chords_ptr, *chords_head;
extern Keys *keys_head, *keys_tail;
extern Key *key_ptr, *key_head;
extern FreeList *free_head, *free_tail;

/* misc */
extern char opt_flag;
extern int redir_fd;
extern int keys_count;
extern unsigned int delim_len;
extern unsigned long Flags;

/* User settings */
extern char command_char;
extern char comment_char;
extern char level_char;
extern char *delim;
extern unsigned int parse_binds;
extern unsigned int parse_conf;
extern unsigned int parse_keys;
extern unsigned int cols;
extern int wkx_x;
extern int wkx_y;
extern int wkx_w;
extern unsigned int border_w;
extern char *col_fg;
extern char *col_bg;
extern char *col_bd;
extern char *def_redir;
extern char *daemon_redir;
extern char *shell;
extern const Key keys[];
extern const Chord chords[];
extern const char *fonts[];
extern char *Fonts[MAXLEN];
extern char *Colors[SchemeLast][3];
extern size_t fonts_len;
extern size_t keys_len;


void define_globals(void);
int opts_handler(void);
void cleanup(void);
void early_free_binds(Bind *b);
void free_binds(Bind *b);
void free_chords(Chord *curr);
void free_cmd(char **cmd);
void free_fonts(FontList *f);
void free_keys_list(Keys *curr);
void free_keys_array(Key *curr);
void print_binds(Bind *b);
void print_chords(Chord *curr, int indent);
void print_str_array(char *arr[]);

#endif /* WKX_H_ */
