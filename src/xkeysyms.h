/* See LICENSE file for copyright and license details. */
#ifndef WKX_KEYS_H
#define WKX_KEYS_H

#include <stdint.h>
#include <stdlib.h>

typedef struct KeysDict KeysDict;
struct KeysDict {
    char *p_name;
    char *keyname;
    uint32_t keysym;
};

extern const KeysDict kd[];
extern const size_t xf86_len;

#endif /* WKX_KEYS_H */
