/* See LICENSE file for copyright and license details. */
#include "helpers.h"
#include <locale.h>
#include <time.h>

#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/Xutil.h>
#ifdef XINERAMA
#include <X11/extensions/Xinerama.h>
#endif
#include <X11/Xft/Xft.h>

#include "drw.h"
#include "util.h"
#include "wkx.h"
#include "xwin.h"

/* macros */
#define TEXTW(X)              (drw_fontset_getwidth(drw, (X)) + lrpad)
#define INTERSECT(x,y,w,h,r)  (MAX(0, MIN((x)+(w),(r).x_org+(r).width)  - MAX((x),(r).x_org)) \
                             * MAX(0, MIN((y)+(h),(r).y_org+(r).height) - MAX((y),(r).y_org)))
#define FILTER_CTRL          Button1Mask|Button2Mask|Button3Mask|\
                             Button4Mask|Button5Mask|ShiftMask|\
                             LockMask|Mod1Mask|Mod2Mask|\
                             Mod3Mask|Mod4Mask|Mod5Mask


/* enum for Special keys */
enum { Tab      , S_Tab     ,
       Spc      , S_Spc     ,
       Ret      , S_Ret     ,
       Esc      , S_Esc     ,
       Home     , S_Home    ,
       Left     , S_Left    ,
       Right    , S_Right   ,
       Up       , S_Up      ,
       Down     , S_Down    ,
       PgUp     , S_PgUp    ,
       PgDown   , S_PgDown  ,
       End      , S_End     ,
       Begin    , S_Begin   ,
       Del      , S_Del     ,
       LastKey };

static int lh, mw, mh; /* Not bh but lh as there is no bar just line */
static int lrpad;      /* sum of left and right padding */
static int mon = -1, screen;

static Atom clip, utf8;
static Display *dpy;
static Window root, parentwin, win;
static XIC xic;

static Drw *drw;
static Clr *scheme[SchemeLast];

static Chord *head, *current, *prev;

static int screen;
static int ncols;

static char *special_keys[LastKey] = {
    [Tab]    = "TAB"    , [S_Tab]    = "S-TAB"    ,
    [Spc]    = "SPC"    , [S_Spc]    = "S-SPC"    ,
    [Ret]    = "RET"    , [S_Ret]    = "S-RET"    ,
    [Esc]    = "ESC"    , [S_Esc]    = "S-ESC"    ,
    [Home]   = "Home"   , [S_Home]   = "S-Home"   ,
    [Left]   = "Left"   , [S_Left]   = "S-Left"   ,
    [Right]  = "Right"  , [S_Right]  = "S-Right"  ,
    [Up]     = "Up"     , [S_Up]     = "S-Up"     ,
    [Down]   = "Down"   , [S_Down]   = "S-Down"   ,
    [PgUp]   = "PgUp"   , [S_PgUp]   = "S-PgUp"   ,
    [PgDown] = "PgDown" , [S_PgDown] = "S-PgDown" ,
    [End]    = "End"    , [S_End]    = "S-End"    ,
    [Begin]  = "Begin"  , [S_Begin]  = "S-Begin"  ,
    [Del]    = "DEL"    , [S_Del]    = "S-DEL"
};

void
keypress(XKeyEvent *ev)
{
    PUTS("\nFunction: keypress");
    char buf[32];
    KeySym ksym;
    Status status;
    int proc_stat;
    unsigned int state;
    state = ev->state;
    ev->state &= FILTER_CTRL;
    XmbLookupString(xic, ev, buf, sizeof(buf), &ksym, &status);
    switch (status) {
    default: /* XLookupNone, XBufferOverflow */
        return;
    case XLookupChars:
    case XLookupKeySym:
    case XLookupBoth:
        break;
    }

    switch(ksym) {
    default:
        if (!specialkey(buf, ksym, state) &&
            iscntrl((unsigned char)*buf))
            break;
        proc_stat = process_keypress(buf, state);
        if (proc_stat < 0) {
            x_cleanup();
            exit(EXIT_FAILURE);
        } else if (!proc_stat) {
            x_cleanup();
            exit(EXIT_SUCCESS);
        }
        break;
    }
}

int
process_keypress(char *buf, unsigned int mod_flag)
{
    Chord *curr;
    char mod_chars[32];
    int mod_count = 0;
    if (mod_flag & ControlMask) {
        mod_chars[mod_count++] = 'C';
        mod_chars[mod_count++] = '-';
    }
    if (mod_flag & Mod1Mask) {
        mod_chars[mod_count++] = 'M';
        mod_chars[mod_count++] = '-';
    }
    if (mod_flag & Mod4Mask) {
        mod_chars[mod_count++] = 'H';
        mod_chars[mod_count++] = '-';
    }
    mod_chars[mod_count] = '\0';
    if (mod_chars[0] != '\0') {
        char tmp[32];
        int i, j;
        for (i = 0; buf[i] != '\0'; ++i)
            tmp[i] = buf[i];
        tmp[i] = buf[i];
        if (i > 32 - mod_count) {
            warn("WARNING: Too many keys pressed with modifier.\n");
            return -1;
        }
        for (j = 0; mod_chars[j] != '\0'; ++j)
            buf[j] = mod_chars[j];
        for (i = 0; tmp[i] != '\0'; ++i, ++j)
            buf[j] = tmp[i];
        buf[j] = tmp[i];
    }
    PRINTF("\tbuf.......'%s'\n", buf);
    if ((curr = find_key(buf, current)) == NULL)
        return -1;
    if (curr->chords != NULL) {
        prev = current;
        current = curr->chords;
        unsigned int lines;
        unsigned int items;
        items = calc_items(current);
        calc_cols(items);
        lines = calc_lines(items);
        mh = lines * lh;
        XWindowChanges wc;
        wc.height = mh;
        XConfigureWindow(dpy, win, CWHeight, &wc);
        drw_resize(drw, mw, mh);
        drawmenu(current, items, lines);
        return 1;
    } else if (curr->cmd != NULL) {
        spawn(curr->cmd);
        return 0;
    }
    return -1;
}

int
specialkey(char *buf, KeySym ksym, unsigned int state)
{
    PUTS("Function: specialkey");
    char *tmp;
    switch (ksym) {
    case XK_Tab:
    case XK_KP_Tab:
        tmp = special_keys[Tab];
        break;
    case XK_ISO_Left_Tab:
        tmp = special_keys[S_Tab];
        break;
    case XK_space:
    case XK_KP_Space:
        if (state & ShiftMask)
            tmp = special_keys[S_Spc];
        else
            tmp = special_keys[Spc];
        break;
    case XK_Return:
    case XK_KP_Enter:
        if (state & ShiftMask)
            tmp = special_keys[S_Ret];
        else
            tmp = special_keys[Ret];
        break;
    case XK_Escape:
        if (state & ShiftMask)
            tmp = special_keys[S_Esc];
        else
            tmp = special_keys[Esc];
        break;
    case XK_Home:
    case XK_KP_Home:
        if (state & ShiftMask)
            tmp = special_keys[S_Home];
        else
            tmp = special_keys[Home];
        break;
    case XK_Left:
    case XK_KP_Left:
        if (state & ShiftMask)
            tmp = special_keys[S_Left];
        else
            tmp = special_keys[Left];
        break;
    case XK_Up:
    case XK_KP_Up:
        if (state & ShiftMask)
            tmp = special_keys[S_Up];
        else
            tmp = special_keys[Up];
        break;
    case XK_Right:
    case XK_KP_Right:
        if (state & ShiftMask)
            tmp = special_keys[S_Right];
        else
            tmp = special_keys[Right];
        break;
    case XK_Down:
    case XK_KP_Down:
        if (state & ShiftMask)
            tmp = special_keys[S_Down];
        else
            tmp = special_keys[Down];
        break;
    case XK_Page_Up:
    case XK_KP_Page_Up:
        if (state & ShiftMask)
            tmp = special_keys[S_PgUp];
        else
            tmp = special_keys[PgUp];
        break;
    case XK_Page_Down:
    case XK_KP_Page_Down:
        if (state & ShiftMask)
            tmp = special_keys[S_PgDown];
        else
            tmp = special_keys[PgDown];
        break;
    case XK_End:
    case XK_KP_End:
        if (state & ShiftMask)
            tmp = special_keys[S_End];
        else
            tmp = special_keys[End];
        break;
    case XK_Begin:
    case XK_KP_Begin:
        if (state & ShiftMask)
            tmp = special_keys[S_Begin];
        else
            tmp = special_keys[Begin];
        break;
    case XK_Delete:
    case XK_KP_Delete:
        if (state & ShiftMask)
            tmp = special_keys[S_Del];
        else
            tmp = special_keys[Del];
        break;
    default:
        return 0;
    }
    int i;
    for (i = 0; tmp[i] != '\0'; ++i)
        buf[i] = tmp[i];
    buf[i] = tmp[i];
    PRINTF("\tspecial......'%s'\n", buf);
    return 1;
}

void
calc_cols(unsigned int i)
{
    PUTS("\nFunction: calc_cols");
    if (i <= cols) {
        ncols = i;
        goto end;
    }
    unsigned int lines, min_lines;
    unsigned int min_cols;
    unsigned int items;
    min_lines = i;
    min_cols = 0;
    for (ncols = 2; ncols <= cols; ++ncols) {
        items = i;
        lines = (items % ncols) ? 1 : 0;
        while (items / ncols) {
            lines += items / ncols;
            items %= ncols;
        }
        if (lines < min_lines) {
            min_cols = ncols;
            min_lines = lines;
        }
    }
    if (min_cols < ncols)
        ncols = min_cols;
    if (ncols > cols) /* TODO test, if this is needed */
        ncols = cols;
end:
    PRINTF("\tcols........'%u'\n", ncols);
    return;
}

unsigned int
calc_lines(unsigned int i)
{
    PUTS("\nFunction: calc_lines");
    unsigned int lines;
    lines = (i % ncols) ? 1 : 0;
    while (i / ncols) {
        lines += i / ncols;
        i %= ncols;
    }
    PRINTF("\tlines.....'%i'\n", lines);
    return lines;
}

unsigned int
calc_items(Chord *curr)
{
    unsigned int i;
    for (i = 0; curr->key != NULL; ++i, ++curr)
        ;
    return i;
}

int
drawitem(Chord *curr, int x, int y, int w)
{
    PUTS("\nFunction: drawitem");
    size_t i = 3; /* count two spaces for delim, and '\0'*/
    i += strlen(curr->key);
    i += strlen(curr->desc);
    i += delim_len;
    char text[i];
    sprintf(text, "%s %s %s", curr->key, delim, curr->desc);
    PRINTF("\t%s\n", text);
    return drw_text(drw, x, y, w, lh, lrpad / 2, text, 0);
}

void
drawmenu(Chord *curr, unsigned int items, unsigned int lines)
{
    PUTS("\nFunction: drawmenu");
    Chord *c;
    drw_setscheme(drw, scheme[SchemeNorm]);
    drw_rect(drw, 0, 0, mw, mh, 1, 1);
    drw_rect(drw, 2, 2, 2, lh - 4, 1, 0);
    if (items == ~0)
        items = calc_items(curr);
    if (lines == ~0)
        lines = calc_lines(items);

    /* draw grid */
    int i = 0;
    for (c = curr; c->key != NULL; ++c, ++i) {
        drawitem(
            c,
            ((i / lines) * (mw / ncols)),
            (i % lines) * lh,
            ((mw / ncols) - 4)
        );
    }
    drw_map(drw, win, 0, 0, mw, mh);
}

void
grabfocus(void)
{
    PUTS("\nFunction: grabfocus");
    struct timespec ts = { .tv_sec = 0, .tv_nsec = 10000000  };
    Window focuswin;
    int i, revertwin;

    for (i = 0; i < 100; ++i) {
        XGetInputFocus(dpy, &focuswin, &revertwin);
        if (focuswin == win)
            return;
        XSetInputFocus(dpy, win, RevertToParent, CurrentTime);
        nanosleep(&ts, NULL);
    }
    x_cleanup();
    err("ERROR: Could not grab focus\n");
}

void
grabkeyboard(void)
{
    PUTS("\nFunction: grabkeyboard");
    struct timespec ts = { .tv_sec = 0, .tv_nsec = 1000000  };
    int i;

    /* try to grab keyboard, we may have to wait for another process to ungrab */
    for (i = 0; i < 1000; i++) {
        if (XGrabKeyboard(dpy, DefaultRootWindow(dpy), True,
                          GrabModeAsync, GrabModeAsync,
                          CurrentTime) == GrabSuccess)
            return;
        nanosleep(&ts, NULL);
    }
    x_cleanup();
    err("ERROR: Could not grab keyboard\n");
}

void
run(void)
{
    PUTS("\nFunction: run");
    XEvent ev;

    while (!XNextEvent(dpy, &ev)) {
        if (XFilterEvent(&ev, win))
            continue;
        switch(ev.type) {
        case DestroyNotify:
            if (ev.xdestroywindow.window != win)
                break;
            x_cleanup();
            exit(1);
        case Expose:
            if (ev.xexpose.count == 0)
                drw_map(drw, win, 0, 0, mw, mh);
            break;
        case FocusIn:
            /* regrab focus from parent window */
            if (ev.xfocus.window != win)
                grabfocus();
            break;
        case KeyPress:
            keypress(&ev.xkey);
            break;
        case VisibilityNotify:
            if (ev.xvisibility.state != VisibilityUnobscured)
                XRaiseWindow(dpy, win);
            break;
        }
    }
}

void
setup(void)
{
    PUTS("\nFunction: setup");
    int x, y; /* x and y of left window corner */
    int i, j; /* counters */
    unsigned int lines;
    unsigned int items;
    XSetWindowAttributes swa;
    XIM xim;
    XWindowAttributes wa;
    XClassHint ch = {"wkx", "wkx"};
#ifdef XINERAMA
    unsigned int du;
    Window w, dw, *dws;
    XineramaScreenInfo *info;
    Window pw;
    int a, di, n, area = 0;
#endif
    /* init appearance */
    for (j = 0; j < SchemeLast; j++)
        scheme[j] = drw_scm_create(drw, (const char **)Colors[j], 3);

    clip = XInternAtom(dpy, "CLIPBOARD",   False);
    utf8 = XInternAtom(dpy, "UTF8_STRING", False);

    /* calculate menu geometry */
    lh = drw->fonts->h + 2;
    items = calc_items(head);
    calc_cols(items);
    lines = calc_lines(items);
    mh = lines * lh;
#ifdef XINERAMA
    i = 0;
    if (parentwin == root && (info = XineramaQueryScreens(dpy, &n))) {
        XGetInputFocus(dpy, &w, &di);
        if (mon >= 0 && mon < n)
            i = mon;
        else if (w != root && w != PointerRoot && w != None) {
            /* find top-level window containing current input focus */
            do {
                if (XQueryTree(dpy, (pw = w), &dw, &w, &dws, &du) && dws)
                    XFree(dws);
            } while (w != root && w != pw);
            /* find xinerama screen with which the window intersects most */
            if (XGetWindowAttributes(dpy, pw, &wa))
                for (j = 0; j < n; j++)
                    if ((a = INTERSECT(wa.x, wa.y, wa.width, wa.height, info[j])) > area) {
                        area = a;
                        i = j;
                    }
        }
        /* no focused window is on screen, so use pointer location instead */
        if (mon < 0 && !area && XQueryPointer(dpy, root, &dw, &dw, &x, &y, &di, &di, &du))
            for (i = 0; i < n; i++)
                if (INTERSECT(x, y, 1, 1, info[i]) != 0)
                    break;

        x = (wkx_x > -1 ? wkx_x : info[i].x_org + info[i].width / 4);
        y = (wkx_y > -1 ? wkx_y :
             info[i].y_org + info[i].height - mh - (info[i].height / 10));
        if (wkx_w < 0)
            mw = info[i].width / 2;
        else if (!wkx_w)
            mw = info[i].width;
        else
            mw = wkx_w;
        XFree(info);
    } else
#endif
    {
        if (!XGetWindowAttributes(dpy, parentwin, &wa)) {
            x_cleanup();
            err("could not get embedding window attributes: 0x%lx",
                parentwin);
        }
        x = (wkx_x > -1 ? wkx_x : wa.width / 4);
        y = wa.height - mh - wkx_y;
        if (wkx_w < 0)
            mw = wa.width / 2;
        else if (!wkx_w)
            mw = wa.width;
        else
            mw = wkx_w;
    }

    /* create menu window */
    swa.override_redirect = True;
    swa.background_pixel = scheme[SchemeNorm][ColBg].pixel;
    swa.event_mask = ExposureMask | KeyPressMask | VisibilityChangeMask;
    win = XCreateWindow(dpy, parentwin, x, y - border_w * 2,
                        mw - border_w * 2, mh, border_w,
                        CopyFromParent, CopyFromParent, CopyFromParent,
                        CWOverrideRedirect | CWBackPixel | CWEventMask,
                        &swa);
    if (border_w)
        XSetWindowBorder(dpy, win,
                         scheme[SchemeNorm][ColBorder].pixel);
    XSetClassHint(dpy, win, &ch);


    /* input methods */
    if ((xim = XOpenIM(dpy, NULL, NULL, NULL)) == NULL) {
        x_cleanup();
        err("XOpenIM failed: could not open input device");
    }

    xic = XCreateIC(xim, XNInputStyle,
                    XIMPreeditNothing | XIMStatusNothing,
                    XNClientWindow, win, XNFocusWindow, win, NULL);

    XMapRaised(dpy, win);
    drw_resize(drw, mw, mh);
    drawmenu(head, items, lines);
}

int
xwin(Chord *curr)
{
    XWindowAttributes wa;
    head = current = prev = curr;

    if (!setlocale(LC_CTYPE, "") || !XSupportsLocale())
        warn("WARNING: Locale not supported\n");
    if (!(dpy = XOpenDisplay(NULL))) {
        x_cleanup();
        err("ERROR: Cannot open display.\n");
    }
    screen    = DefaultScreen(dpy);
    root      = RootWindow(dpy, screen);
    parentwin = root;
    if (!XGetWindowAttributes(dpy, parentwin, &wa)) {
        x_cleanup();
        err("ERROR: Could not get window attributes\n");
    }
    drw = drw_create(dpy, screen, root, wa.width, wa.height);
    if (!drw_fontset_create(drw, (const char **)Fonts, fonts_len)) {
        x_cleanup();
        err("ERROR: Could not load fonts\n");
    }

    lrpad = drw->fonts->h;
    grabkeyboard();
    setup();
    run();
    return 1;
}

void
x_cleanup(void)
{
    XUngrabKey(dpy, AnyKey, AnyModifier, root);
    free(scheme[SchemeNorm]);
    drw_free(drw);
    XSync(dpy, False);
    XCloseDisplay(dpy);
    FcFini();
    cleanup();
    return;
}

