/* See LICENSE file for copyright and license details. */
#ifndef WKX_PARSER_H_
#define WKX_PARSER_H_

#include "types.h"
#include "wkx.h"

extern unsigned int font_count;
extern FontList *font_list_head;

/***************
 * binds funcs *
 ***************/
void parse_binds_file(void);
void define_binds_file(void);
int load_binds_file(const char *binds_file);
int parse_binds_line(const char *str);
Bind *make_bind(void);
void tally_binds_list(void);
Bind *sub_tally(Bind *curr, int indent);
Chord *convert_binds_list(Bind *curr);
int write_binds_file(const char *out_file);
int write_binds_header(FILE *__restrict fp);
int write_binds_line(FILE *__restrict fp, Chord *curr, int indent);
int write_binds_footer(FILE *__restrict fp);

/**************
 * conf funcs *
 **************/
void parse_conf_file(void);
void define_conf_file(void);
int load_conf_file(const char *config_file);
int parse_conf_line(const char *str);
int assign_char(char *opt, const char *val);
int assign_int(int *opt, const char *val);
int assign_str(char **opt, const char *val);
int assign_fnt(char **opt, const char *val);
FontList *make_font_list(void);
void define_colors(void);
void define_fonts(char *font[]);

/*************
 * key funcs *
 *************/
void parse_key_file(void);
void define_keys_file(void);
int load_keys_file(const char *keys_file);
int parse_keys_line(char *str);
int get_key(char **start, char **end);
unsigned int get_mod(char *start, int len);
KeySym xf86_str_to_keysym(char *str);
Keys *make_keys(void);
Key *convert_keys_list(Keys *curr);
int write_keys_file(const char *out_file);
int write_keys_header(FILE *__restrict fp);
int write_keys_line(FILE *__restrict fp, Keys *curr);
int write_keys_footer(FILE *__restrict fp);
void x_mod_to_str(unsigned int mod, char *tmp);
char *xf86_keysym_to_string(KeySym ksym);

/**************
 * misc funcs *
 **************/
char *trim_whitespace(char *str);
char *clean_spaces(char *str);
void define_output_file(void);
char *esc_str(char desc[], char *c_desc);
int append_to_free_list(void *item, void (*fp)());
FreeList *make_free_list(void);

/* Copyright Paul Silisteanu for this function
 * https://solarianprogrammer.com/2019/04/03/c-programming-read-file-lines-fgets-getline-implement-portable-getline/
 */
int64_t my_getline(char **__restrict line, size_t *__restrict len, FILE *__restrict fp);

#endif /* WKX_PARSER_H_ */
