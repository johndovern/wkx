/* See LICENSE file for copyright and license details. */
#include <locale.h>
#include <signal.h>
#include <X11/Xutil.h>

#include "helpers.h"
#include "parser.h"
#include "wkx.h"
#include "xdaemon.h"

static unsigned int numlockmask = 0;
static int grabbed;
static int running;
static int toggle_grab;
static int reload;
static Display *dpy;
static Window root;
static int screen;

void
updatenumlockmask(void)
{
    unsigned int i, j;
    XModifierKeymap *modmap;

    numlockmask = 0;
    modmap = XGetModifierMapping(dpy);
    for (i = 0; i < 8; i++)
        for (j = 0; j < modmap->max_keypermod; j++)
            if (modmap->modifiermap[i * modmap->max_keypermod + j]
                == XKeysymToKeycode(dpy, XK_Num_Lock))
                numlockmask = (1 << i);
    XFreeModifiermap(modmap);
}

void
bind_keys(void)
{
    updatenumlockmask();
    Key *kp = key_ptr;
    int i, j, k, len;
    unsigned int modifiers[] = { 0, LockMask, numlockmask,
                                 numlockmask|LockMask };
    int start, end, skip;
    KeySym *syms;

    XUngrabKey(dpy, AnyKey, AnyModifier, root);
    XDisplayKeycodes(dpy, &start, &end);
    syms = XGetKeyboardMapping(dpy, start, end - start + 1, &skip);
    if (!syms) {
        err("ERROR: Could not get syms while grabbing keys");
        return;
    }
    len = LENGTH(modifiers);
    for (k = start; k <= end; ++k)
        for (i = 0, kp = key_ptr; i < keys_len; ++i, ++kp) {
            if (kp->key == syms[(k - start) * skip]) {
                PRINTF("Grabbing......'%s'\n", XKeysymToString(kp->key));
                for (j = 0; j < len; ++j) {
                    XGrabKey(dpy, k, kp->mod|modifiers[j], root, False,
                            GrabModeAsync, GrabModeAsync);
                }
            }
        }
    grabbed = 1;
}

void
key_press(XKeyEvent *ev)
{
    KeySym ksym = XKeycodeToKeysym(dpy, (KeyCode)ev->keycode, 0);
    int i;
    Key *k = key_ptr;
    for (i = 0; i < keys_len; ++i, ++k) {
        if (ksym == k->key &&
            CLEANMASK(k->mod) == CLEANMASK(ev->state)) {
            daemon_spawn(k->cmd, dpy);
        }
    }
}

int
error_fn(Display *d, XErrorEvent *xe)
{
    switch (xe->error_code) {
    case BadAccess:
        warn("WARNING: [BadAccess] key already grabbed.\n");
        return 0;
    }

    warn("WARNING: Unknown error in wkx daemon.\n");
    return 1;
}

void
d_cleanup(void)
{
    XUngrabKey(dpy, AnyKey, AnyModifier, root);
    XSync(dpy, False);
    XCloseDisplay(dpy);
}

void
hold(int sig)
{
    if (sig == SIGHUP || sig == SIGINT || sig == SIGTERM) {
        running = 0;
    }
    else if (sig == SIGUSR1) {
        reload = 1;
    }
    else if (sig == SIGUSR2) {
        toggle_grab = 1;
    }
}

void
reload_cmd(void)
{
    XUngrabKey(dpy, AnyKey, AnyModifier, root);
    XSync(dpy, False);
    parse_key_file();
    XSelectInput(dpy, root, KeyPressMask);
    bind_keys();
    XSync(dpy, False);
    reload = 0;
}

void
toggle_grab_cmd(void)
{
    if (grabbed) {
        XUngrabKey(dpy, AnyKey, AnyModifier, root);
        XSync(dpy, False);
        grabbed = 0;
    } else {
        XSelectInput(dpy, root, KeyPressMask);
        bind_keys();
        XSync(dpy, False);
    }
}

int
start_daemon(void)
{
    running = 1;
    reload = toggle_grab = 0;

    XSetErrorHandler(error_fn);
    if (!setlocale(LC_CTYPE, "") || !XSupportsLocale())
        warn("WARNING: Locale not supported\n");
    if (!(dpy = XOpenDisplay(NULL))) {
        d_cleanup();
        err("ERROR: Cannot open display.\n");
    }

    screen = DefaultScreen(dpy);
    root = RootWindow(dpy, screen);
    XSelectInput(dpy, root, KeyPressMask);
    bind_keys();

    signal(SIGINT,  hold);
    signal(SIGHUP,  hold);
    signal(SIGTERM, hold);
    signal(SIGUSR1, hold);
    signal(SIGUSR2, hold);

    int fd = XConnectionNumber(dpy);
    fd_set descriptors;
    XEvent ev;
    XSync(dpy, False);
    while (running) {
        FD_ZERO(&descriptors);
        FD_SET(fd, &descriptors);
        if (select(fd + 1, &descriptors, NULL, NULL, NULL) > 0) {
            if (XCheckMaskEvent(dpy, KeyPressMask, &ev))
                switch (ev.type) {
                case KeyPress:
                    key_press(&ev.xkey);
                    break;
                }
        }
        if (reload) {
            signal(SIGUSR1, hold);
            reload_cmd();
            reload = 0;
        }
        if (toggle_grab) {
            signal(SIGUSR2, hold);
            toggle_grab_cmd();
            toggle_grab = 0;
        }
    }

    d_cleanup();
    return 1;
}
