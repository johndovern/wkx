/* See LICENSE file for copyright and license details. */
#ifndef WKX_XDAEMON_H
#define WKX_XDAEMON_H

#include <X11/Xlib.h>

#define CLEANMASK(mask)         (mask & ~(numlockmask|LockMask) & (ShiftMask|ControlMask|Mod1Mask|Mod2Mask|Mod3Mask|Mod4Mask|Mod5Mask))

void updatenumlockmask(void);
void bind_keys(void);
void key_press(XKeyEvent *ev);
int error_fn(Display *d, XErrorEvent *xe);
void d_cleanup(void);
void hold(int sig);
void reload_cmd(void);
void toggle_grab_cmd(void);
int start_daemon(void);

#endif /* WKX_XDAEMON_H */
