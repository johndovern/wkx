/* See LICENSE file for copyright and license details. */
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <X11/Xutil.h>

#include "helpers.h"
#include "parser.h"
#include "types.h"
#include "wkx.h"
#include "xkeysyms.h"

enum { Char_ptr, Int_ptr, Str_ptr };
static const Opts opts[] = {
    { "level_char",   &level_char,   Char_ptr, { .c = assign_char } },
    { "comment_char", &comment_char, Char_ptr, { .c = assign_char } },
    { "command_char", &comment_char, Char_ptr, { .c = assign_char } },
    { "delim",        &delim,        Str_ptr,  { .s = assign_str } },
    { "parse_conf",   &parse_conf,   Int_ptr,  { .i = assign_int } },
    { "parse_binds",  &parse_binds,  Int_ptr,  { .i = assign_int } },
    { "cols",         &cols,         Int_ptr,  { .i = assign_int } },
    { "wkx_x",        &wkx_x,        Int_ptr,  { .i = assign_int } },
    { "wkx_y",        &wkx_y,        Int_ptr,  { .i = assign_int } },
    { "wkx_w",        &wkx_w,        Int_ptr,  { .i = assign_int } },
    { "border_w",     &border_w,     Int_ptr,  { .i = assign_int } },
    { "col_fg",       &col_fg,       Str_ptr,  { .s = assign_str } },
    { "col_bg",       &col_bg,       Str_ptr,  { .s = assign_str } },
    { "col_bd",       &col_bd,       Str_ptr,  { .s = assign_str } },
    { "def_redir",    &def_redir,    Str_ptr,  { .s = assign_str } },
    { "daemon_redir", &daemon_redir, Str_ptr,  { .s = assign_str } },
    { "shell",        &shell,        Str_ptr,  { .s = assign_str } },
    { "font",         NULL,          Str_ptr,  { .s = assign_fnt } },
};

unsigned int font_count = 0;
FontList *font_list_head, *font_list_tail;

/*****************
 * bindsrc funcs *
 *****************/
void
parse_binds_file(void)
{
    /* Binds parsing */
    if (parse_binds) {
        define_binds_file();
        if (!load_binds_file(binds_file)) {
            if (opt_flag == 'w') {
                err("ERROR: Binds file '%s' does not exist\n",
                    binds_file);
            } else {
                warn("WARNING: bindsrc '%s' does not exist\n",
                     binds_file);
                binds_file[0] = '\0';
                chords_ptr = (Chord *)chords;
            }
        } else {
            tally_binds_list();
            chords_ptr = convert_binds_list(binds_head);
            if (append_to_free_list(chords_head, free_chords) < 0) {
                cleanup();
                err("ERROR: Could not allocate memory for free list\n");
            }
            /* Flags |= CHORD_FLAG; */
            free_binds(binds_head);
        }
    } else {
        binds_file[0] = '\0';
        chords_ptr = (Chord *)chords;
    }

    chords_head = chords_ptr;
    delim_len = strlen(delim);
    return;
}

void
define_binds_file(void)
{
    PUTS("\nFunction: define_binds_file");
    if (binds_path == NULL) {
        char *config_home = getenv(CONFIG_HOME_ENV);
        if (config_home != NULL)
            snprintf(binds_file, FILE_SIZE, "%s/%s", config_home, BINDS_PATH);
        else
            snprintf(binds_file, FILE_SIZE, "%s/%s/%s", getenv("HOME"), ".config", BINDS_PATH);
    } else {
        if (binds_path[0] != '/') {
            PRINTF("\tbinds_path looks relative: %s\n\tPreappending cwd\n", binds_path);
            if (getcwd(cwd, CWD_SIZE) == NULL) {
                cleanup();
                err("\nError getting cwd...exiting.\n");
            }
            PRINTF("\tcwd detected as %s\n", cwd);
            snprintf(binds_file, FILE_SIZE, "%s/%s", cwd, binds_path);
        } else {
            snprintf(binds_file, FILE_SIZE, "%s", binds_path);
        }
    }
    PRINTF("\tbinds_path = %s\n", binds_path);
    PRINTF("\tbinds_file = %s\n", binds_file);
}

int
load_binds_file(const char *bindsrc_file)
{
    PUTS("\nFunction: load_binds_file");
    PRINTF("\tbinds_file: '%s'\n", bindsrc_file);
    FILE *bfp = fopen(bindsrc_file, "r");
    if (bfp == NULL)
        return 0;

    if (!(binds_head = binds_tail = make_bind())) {
        fclose(bfp);
        cleanup();
        err("ERROR: Could not create binds list.\n");
    }
    if (append_to_free_list(binds_head, early_free_binds) < 0) {
        cleanup();
        fclose(bfp);
        err("ERROR: Could not allocate memory for free list\n");
    }

    char *line = NULL;
    size_t line_size = 0;
    int p_err;
    int i = 1;

    while (my_getline(&line, &line_size, bfp) > -1) {
        if ((*line) == comment_char) {
            ++i;
            continue;
        }
        if ((p_err = parse_binds_line(line)) < 0 ) {
            /* free and clean up */
            cleanup();
            fclose(bfp);
            free(line);
            switch (p_err) {
            case -1:
                err("\nERROR: malloc failure while parsing binds file.\n");
                break;
            case -2:
                err("\nERROR: first level on line '%i' should be zero in bindsrc file: '%s'\n", i, bindsrc_file);
                break;
            case -3:
                err("\nERROR: line '%i' is jumping levels in bindsrc file: '%s'\n", i, bindsrc_file);
                break;
            }
            break;
        }
        /* Create the next bind node */
        if (binds_tail->level > -1) {
            Bind *b;
            if (!(b = make_bind())) {
                cleanup();
                fclose(bfp);
                free(line);
                err("\nERROR: Failed to create next bind.\n");
                break;
            }
            PRINTF("\tb->level......'%i'\n", binds_tail->level);
            PRINTF("\tb->key........'%s'\n", binds_tail->key);
            PRINTF("\tb->desc.......'%s'\n", binds_tail->desc);
            PRINTF("\tb->command....'%s'\n", binds_tail->cmd);
            binds_tail->next = b;
            b->prev = binds_tail;
            binds_tail = b;
        }
        ++i;
    }

    if (binds_tail->level < 0) {
        binds_tail = binds_tail->prev;
        free(binds_tail->next);
        binds_tail->next = NULL;
    }

    switch (errno) {
    case EINVAL: /* FALLTHROUGH */
    case ENOMEM:
    case EOVERFLOW:
        cleanup();
        fclose(bfp);
        free(line);
        err("Failed to parse binds file: '%s'\nError in my_getline: '%i'",
            bindsrc_file, errno);
        break;
    }

    fclose(bfp);
    free(line);
    return 1;
}

int
parse_binds_line(const char *str)
{
    PUTS("\nFunction: parse_binds_line");
    PRINTF("\tstr........'%s'\n", str);
    static int last_level = -1;

    /* Test blank lines */
    size_t len = strlen(str);
    if (!len) {
        PUTS("\tSkipping blank line.");
        return 0;
    }

    /* Line is not blank, copy str into an array */
    char str_cpy[len + 1];
    char *s;
    strcpy(str_cpy, str);
    s = trim_whitespace(str_cpy);

    /* Skip comments */
    if ((*s) == comment_char) {
        PUTS("\tSkipping comment");
        return 0;
    } else if ((*s) == '\0') {
        PUTS("\tSkipping blank line.");
        return 0;
    }

    /* Found a line that isn't a comment or blank */
    /* Setup */
    char *start, *end;
    start = end = s;

    /* Get level */
    for (; (*s) == level_char; ++s, end = s) ;

    /* Need to make sure not giving us a malformed binds file */
    if (last_level < 0 && (end - start) > 0) /* First line has ':' */
        return -2;
    else if (last_level + 1 < (end - start)) /* Line has extra ':' */
        return -3;
    last_level = end - start;

    binds_tail->level = end - start;
    /* Get key */
    start = end = s;
    while (!ISSPACE(s))
        end = ++s;
    size_t k_len = end - start;
    char *key;
    if ((key = malloc(sizeof(char) * k_len + 1)) == NULL)
        return -1;
    sprintf(key, "%.*s", (int)(k_len), start);
    binds_tail->key = key;

    /* Start at next char to search for desc */
    start = end = s;
    LSTRIP(s);

    /* Test for top level prefix w/o description */
    if (*s == '\0') {
        PUTS("\tTop level prefix with no description.");
        char *desc = malloc(sizeof(char) * 2);
        if (desc == NULL)
            return -1;
        strcpy(desc, " ");
        binds_tail->desc = desc;
        return 1;
    }

    /* Find command, if any */
    start = end = s;
    int cmd_flag;
    int d_len;
    for (cmd_flag = 0; cmd_flag < 2 && (*s) != '\0'; end = ++s) {
        if ((*s) == command_char)
            switch (cmd_flag) {
            case 1:
                cmd_flag = 2;
                break;
            case 0:
                cmd_flag = 1;
                break;
            }
        else if (cmd_flag)
            cmd_flag = 0;
    }

    /* Test if line is a prefix */
    if (*end == '\0' && cmd_flag < 2) {
        PUTS("\tPrefix");
        --end;
        RSTRIP(end, start);
        d_len = end - start;
        char *desc = malloc(sizeof(char) * (d_len + 1));
        if (desc == NULL)
            return -1;
        sprintf(desc, "%.*s", d_len, start);
        binds_tail->desc = desc;
        return 1;
    }

    /* Trim trailing space for desc */
    for (--end; *end == command_char; --end);
    RSTRIP(end, start);
    d_len = end - start;
    char *desc = malloc(sizeof(char) * (d_len + 1));
    if (desc == NULL)
        return -1;
    sprintf(desc, "%.*s", d_len, start);
    binds_tail->desc = desc;

    if (*s == '\0' && cmd_flag == 2) {
        PUTS("\tEmpty command");
        char *cmd = malloc(sizeof(char) * 2);
        if (cmd == NULL)
            return -1;
        strcpy(cmd, " ");
        binds_tail->cmd = cmd;
        return 1;
    }

    /* Found an actual command */
    LSTRIP(s);
    end = clean_spaces(s);
    int c_len = end - s;
    char *cmd = malloc(sizeof(char) * (c_len + 1));
    if (cmd == NULL)
        return -1;
    strcpy(cmd, s);
    binds_tail->cmd = cmd;

    return 1;
}

Bind *
make_bind(void)
{
    Bind *b = calloc(1, sizeof(Bind));
    if (b == NULL) {
        return b;
    }
    b->count   = -1;
    b->level   = -1;
    b->key     = NULL;
    b->desc    = NULL;
    b->cmd = NULL;
    b->prev    = NULL;
    b->next    = NULL;
    return b;
}

void
tally_binds_list(void)
{
    PUTS("\nFunction: tally_binds_list");
    Bind *curr;
    curr = binds_tail;
    int tally = 0;
    int last_level = curr->level;
    do {
        PRINTF("\tdesc......'%s'\n", curr->desc);
        if (last_level == curr->level) {
            curr->count = ++tally;
        } else if (curr->level < last_level) {
            tally = 1;
            curr->count = tally;
            last_level = curr->level;
        } else {
            curr = sub_tally(curr, 1);
        }
        curr = curr->prev;
    } while (curr != NULL);
    return;
}

Bind *
sub_tally(Bind *curr, int indent)
{
    int level = curr->level;
    int tally = 0;
    Bind *last = curr;
    int i;
    do {
        if (Flags & DEBUG_FLAG)
            for (i = 0; i < indent; ++i)
                printf("\t");
        PRINTF("\tdesc......'%s'\n", curr->desc);
        if (level == curr->level) {
            curr->count = ++tally;
        } else if (curr->level > level) {
            curr = sub_tally(curr, indent + 1);
        }
        if (curr->prev == NULL)
            break;
        last = curr;
        curr = curr->prev;
    } while (!(curr->level < level));
    PUTS(" ");
    return last;
}

Chord *
convert_binds_list(Bind *b_curr)
{
    PUTS("\nFunction: convert_binds_list");

    Chord *chord, *c_curr;
    chord = c_curr = NULL;
    int size = b_curr->count;

    if (size < 1)
        return chord;

    c_curr = chord = malloc(sizeof(Chord) * (size + 1));
    if (chord == NULL) {
        cleanup();
        err("Could not create chord array.\n");
    }

    Chord *last = c_curr;
    int level = b_curr->level;
    while (b_curr != NULL && (c_curr - chord) < size) {
        if (b_curr->level > level) {
            last->chords = convert_binds_list(b_curr);
            while (b_curr != NULL && b_curr->level > level)
                b_curr = b_curr->next;
            if (b_curr == NULL)
                break;
        }
        c_curr->key = b_curr->key;
        c_curr->desc = b_curr->desc;
        PRINTF("\tc_curr->desc...'%s'\n", c_curr->desc);
        c_curr->cmd = b_curr->cmd;
        c_curr->chords = NULL;
        last = c_curr++;
        b_curr = b_curr->next;
    }
    PUTS(" ");
    /* Last Chord may have been a prefix */
    if (b_curr != NULL && b_curr->level > level)
        last->chords = convert_binds_list(b_curr);
    /* Last Chord in array must be null */
    c_curr->key     = NULL;
    c_curr->desc    = NULL;
    c_curr->cmd     = NULL;
    c_curr->chords  = NULL;

    return chord;
}

int
write_binds_file(const char *out_file)
{
    PUTS("\nFunction: write_binds_file");
    PRINTF("out_file: '%s'\n", out_file);
    FILE *ofp = NULL;
    if (out_file[0] == '\0')
        ofp = stdout;
    else
        ofp = fopen(out_file, "w");
    if (ofp == NULL)
        return -1;
    write_binds_header(ofp);
    write_binds_line(ofp, chords_ptr, 1);
    write_binds_footer(ofp);
    fclose(ofp);
    PUTS(" ");
    return 1;
}

int
write_binds_header(FILE *__restrict fp)
{
    fputs(
        "/* See LICENSE file for copyright and license details. */\n"
        "#ifndef WKX_BINDS_H_\n"
        "#define WKX_BINDS_H_\n"
        "\n"
        "#define PREFIX(...) (Chord[]){ __VA_ARGS__, { NULL, NULL, NULL, NULL } }\n"
        "#define CHORDS(...) { __VA_ARGS__, { NULL, NULL, NULL, NULL } }\n"
        "\n"
        "const Chord chords[] = CHORDS(\n"
        "    /* bind     desc            command             prefix */\n",
        fp);
    return 1;
}

int
write_binds_line(FILE *__restrict fp, Chord *curr, int indent)
{
    char tmp[MAXLEN * 2];
    while (curr != NULL && curr->key != NULL) {
        fprintf(fp, "%*c{", indent * 4, ' ');
        fprintf(fp, " \"%s\",\t", esc_str(tmp, curr->key));
        fprintf(fp, "\"%s\",\t", esc_str(tmp, curr->desc));
        if (curr->cmd != NULL)
            fprintf(fp, "\"%s\",\t", esc_str(tmp, curr->cmd));
        else
            fprintf(fp, "NULL,\t\t");
        if (curr->chords != NULL) {
            fprintf(fp, "PREFIX(\n");
            write_binds_line(fp, curr->chords, indent + 1);
            if ((curr + 1)->key != NULL)
                fprintf(fp, "%*c)},\n", indent * 4, ' ');
            else
                fprintf(fp, "%*c)}\n", indent * 4, ' ');
        } else {
            if ((curr + 1)->key != NULL)
                fprintf(fp, "NULL },\n");
            else
                fprintf(fp, "NULL }\n");
        }
        ++curr;
    }
    return 1;
}

int
write_binds_footer(FILE *__restrict fp)
{
    fputs(");\n"
         "\n"
         "#endif /* WKX_BINDS_H_ */\n", fp);
    return 1;
}

/***************
 * wkxrc funcs *
 ***************/
void
parse_conf_file(void)
{
    /* Config parsing */
    if (parse_conf) {
        define_conf_file();
        if (!load_conf_file(config_file))
            warn("WARNING: conf file '%s' does not exist\n",
                 config_file);
    } else {
        config_file[0] = '\0';
    }
    if (font_count) {
        fonts_len = (font_count <= MAXLEN) ? font_count : MAXLEN;
        define_fonts(NULL);
    } else {
        define_fonts((char **)fonts);
    }
    define_colors();
}

void
define_conf_file(void)
{
    PUTS("\nFunction: define_conf_file");
    if (config_path == NULL) {
        char *config_home = getenv(CONFIG_HOME_ENV);
        if (config_home != NULL)
            snprintf(config_file, FILE_SIZE, "%s/%s", config_home, CONFIG_PATH);
        else
            snprintf(config_file, FILE_SIZE, "%s/%s/%s", getenv("HOME"), ".config", CONFIG_PATH);
    } else {
        if (config_path[0] != '/') {
            PRINTF("\tconfig_path looks relative: %s\nPreappending cwd\n", config_path);
            if (getcwd(cwd, CWD_SIZE) == NULL)
                err("\nError getting cwd...exiting.\n");
            PRINTF("\tcwd detected as %s\n", cwd);
            snprintf(config_file, FILE_SIZE, "%s/%s", cwd, config_path);
        } else {
            snprintf(config_file, FILE_SIZE, "%s", config_path);
        }
    }
    PRINTF("\tconfig_path = %s\n", config_path);
    PRINTF("\tconfig_file = %s\n", config_file);
}

int
load_conf_file(const char *config_file)
{
    PUTS("\nFunction: load_conf_file");
    PRINTF("\tconfig_file: '%s'\n", config_file);
    FILE *cfp = fopen(config_file, "r");
    if (cfp == NULL)
        return 0;

    char *line = NULL;
    size_t line_size = 0;
    int p_err;
    int i = 1;
    font_list_head = font_list_tail = NULL;

    if (!(font_list_head = font_list_tail = make_font_list())) {
        fclose(cfp);
        err("ERROR: Could not create font list.\n");
    }
    if (append_to_free_list(font_list_head, free_fonts) < 0) {
        cleanup();
        fclose(cfp);
        err("ERROR: Could not allocate memory for free list\n");
    }

    while (my_getline(&line, &line_size, cfp) > -1){
        if (*line == comment_char)
            continue;
        if ((p_err = parse_conf_line(line)) < 0 ) {
            /* free and clean up */
            fclose(cfp);
            cleanup();
            free(line);
            switch (p_err) {
            case -1:
                err("\nERROR: malloc failure while parsing config file.\n");
                break;
            }
            break;
        }
        /* Create the next font */
        if (font_list_tail->font != NULL) {
            FontList *n;
            if (!(n = make_font_list())) {
                cleanup();
                fclose(cfp);
                free(line);
                err("\nERROR: Failed to create next font.\n");
            }
            font_list_tail->next = n;
            font_list_tail = n;
        }
        ++i;
    }

    switch (errno) {
    case EINVAL: /* FALLTHROUGH */
    case ENOMEM:
    case EOVERFLOW:
        cleanup();
        fclose(cfp);
        free(line);
        err("Failed to parse binds file: '%s'\nError in my_getline: '%i'",
            config_file, errno);
        break;
    }
    /* Check only used fonts are alloced */
    if (font_list_head->font == NULL) {
        FontList *f = font_list_head;
        font_list_head = font_list_head->next;
        free(f);
    }

    fclose(cfp);
    free(line);
    return 1;
}

int
parse_conf_line(const char *str)
{
    PUTS("\nFunction: parse_conf_line");
    PRINTF("\tstr........'%s'\n", str);

    /* Test blank lines */
    size_t len = strlen(str);
    if (!len) {
        PUTS("\tSkipping blank line.");
        return 0;
    }

    /* Copy str into an array */
    char str_cpy[len + 1];
    char *s;
    strcpy(str_cpy, str);
    s = trim_whitespace(str_cpy);

    /* Skip comments/blank lines */
    if ((*s) == '#') {
        PUTS("\tSkipping comment");
        return 0;
    } else if ((*s) == '\0') {
        PUTS("\tSkipping blank line.");
        return 0;
    }

    /* Found a line that isn't a comment or blank */
    /* Setup */
    char opt[len];
    char val[len];
    char *start, *end;
    static size_t opts_len = LENGTH(opts);
    start = end = s;

    while ((*end) != '\0' && !(ISSPACE(end)) && (*end) != '=')
        ++end;

    if ((*end) == '\0') {
        PUTS("\tOpt with no value");
        return 0;
    }
    s = end;
    --end;
    RSTRIP(end, start);

    sprintf(opt, "%.*s", (int)(end - start), start);

    /* if opt in opts then ... */
    int optnum;
    for (optnum = 0; optnum < opts_len; ++optnum)
        if (!(strcmp(opt, opts[optnum].opt)))
            break;

    if (!(optnum < opts_len)) {
        PRINTF("\tNot a valid option: '%s'\n", opt);
        return 0;
    }

    /* Get from = to val */
    start = end = s;
    while ((*s) != '\0' && (ISSPACE(s) || (*s) == '='))
        ++s;
    if ((*s) == '\'' || (*s) == '\"')
        ++s;

    if ((*s) == '\0') {
        PUTS("\tValue is empty");
        return 0;
    }
    start = end = s;

    /* Read value */
    while ((*s) != '\0')
        end = s++;

    if ((*end) == '\'' || (*end) == '\"')
        --end;

    sprintf(val, "%.*s", (int)(end - start + 1), start);

    if (opts[optnum].ptr_val == Char_ptr) {
        return opts[optnum].fp.c(opts[optnum].var, val);
    } else if (opts[optnum].ptr_val == Int_ptr) {
        return opts[optnum].fp.i(opts[optnum].var, val);
    } else if (opts[optnum].ptr_val == Str_ptr) {
        return opts[optnum].fp.s(opts[optnum].var, val);
    }

    return 1;
}

int
assign_char(char *opt, const char *val)
{
    *opt = *val;
    return 1;
}

int
assign_int(int *opt, const char *val)
{
    *opt = atoi(val);
    return 1;
}

int
assign_str(char **opt, const char *val)
{
    *opt = malloc(sizeof(char) * (strlen(val) + 1));
    if (*opt == NULL)
        return -1;
    strcpy(*opt, val);
    if (append_to_free_list(*opt, free) < 0)
        return -1;
    return 1;
}

int
assign_fnt(char **opt, const char *val)
{
    char *bucket;
    bucket = malloc(sizeof(char) * (strlen(val) + 1));
    if (bucket == NULL)
        return -1;
    strcpy(bucket, val);
    font_list_tail->font = bucket;
    ++font_count;
    return 1;
}

FontList *
make_font_list(void)
{
    FontList *f = calloc(1, sizeof(FontList));
    if (f == NULL) {
        return f;
    }
    f->font = NULL;
    f->next = NULL;
    return f;
}

void
define_colors(void)
{
    Colors[SchemeNorm][0] = col_fg;
    Colors[SchemeNorm][1] = col_bg;
    Colors[SchemeNorm][2] = col_bd;
    return;
}

void
define_fonts(char *font[])
{
    if (font == NULL) {
        FontList *f = font_list_head;
        for (int i = 0; i < fonts_len; ++i) {
            Fonts[i] = f->font;
            f = f->next;
        }
    } else {
        char **f = font;
        for (int i = 0; i < fonts_len; ++i)
            Fonts[i] = *f++;
    }
    return;
}

/****************
 * keysrc funcs *
 ****************/
void
parse_key_file(void)
{
    if (parse_keys) {
        define_keys_file();
        if (!load_keys_file(keys_file)) {
            if (opt_flag == 'W') {
                err("ERROR: Keys file '%s' does not exist\n",
                    keys_file);
            } else {
                warn("WARNING: keysrc '%s' does not exist\n",
                     keys_file);
                keys_file[0] = '\0';
                key_ptr = (Key *)keys;
            }
        } else {
            Flags |= KLIST_FLAG;
            if (opt_flag == 'd') {
                key_ptr = convert_keys_list(keys_head);
                keys_len = keys_count;
                Flags |= KARAY_FLAG;
            }
        }
    } else {
        keys_file[0] = '\0';
        key_ptr = (Key *)keys;
    }
    return;
}

void
define_keys_file(void)
{
    PUTS("\nFunction: define_keys_file");
    if (keys_path == NULL) {
        char *config_home = getenv(CONFIG_HOME_ENV);
        if (config_home != NULL)
            snprintf(keys_file, FILE_SIZE, "%s/%s", config_home, KEYS_PATH);
        else
            snprintf(keys_file, FILE_SIZE, "%s/%s/%s", getenv("HOME"), ".config", KEYS_PATH);
    } else {
        if (keys_path[0] != '/') {
            PRINTF("\tkeys_path looks relative: %s\n\tPreappending cwd\n", keys_path);
            if (getcwd(cwd, CWD_SIZE) == NULL) {
                cleanup();
                err("\nError getting cwd...exiting.\n");
            }
            PRINTF("\tcwd detected as %s\n", cwd);
            snprintf(keys_file, FILE_SIZE, "%s/%s", cwd, keys_path);
        } else {
            snprintf(keys_file, FILE_SIZE, "%s", keys_path);
        }
    }
    PRINTF("\tkeys_path = %s\n", keys_path);
    PRINTF("\tkeys_file = %s\n", keys_file);
}

int
load_keys_file(const char *keysrc_file)
{
    PUTS("\nFunction: load_keys_file");
    PRINTF("\tkeys_file: '%s'\n", keysrc_file);
    FILE *kfp = fopen(keysrc_file, "r");
    if (kfp == NULL)
        return 0;

    if (!(keys_head = keys_tail = make_keys())) {
        fclose(kfp);
        cleanup();
        err("ERROR: Could not create keys list.\n");
    }
    if (append_to_free_list(keys_head, free_keys_list) < 0) {
        cleanup();
        fclose(kfp);
        err("ERROR: Could not allocate memory for free list\n");
    }

    char  *line = NULL;
    size_t line_size = 0;
    int p_err;
    int i = 1;
    Keys *last = NULL;
    keys_count = 0;

    while (my_getline(&line, &line_size, kfp) > -1) {
        if ((*line) == comment_char) {
            ++i;
            continue;
        }
        if ((p_err = parse_keys_line(line)) < 0 ) {
            switch (p_err) {
            case -1: /* memory error */
                /* free and clean up */
                cleanup();
                fclose(kfp);
                free(line);
                err("\nERROR: malloc failure while parsing keys file.\n");
                break;
            case -2: /* malformed line */
                warn("\nWARNING: no command on line '%i' in: '%s'\n",
                    i, keysrc_file);
                break;
            case -3: /* bad keysym */
                warn("\nWARNING: bad keysym on line '%i' in: '%s'\n",
                    i, keysrc_file);
                break;
            }
            break;
        }
        /* Create the next bind node */
        if (keys_tail->cmd != NULL) {
            Keys *k;
            if (!(k = make_keys())) {
                cleanup();
                fclose(kfp);
                free(line);
                err("\nERROR: Failed to create next key in list.\n");
                break;
            }
            PUTS(" ");
            PRINTF("\tk->mod........'%i'\n", keys_tail->mod);
            PRINTF("\tk->key........'%zu'\n", keys_tail->key);
            PRINTF("\tk->cmd........'%s'\n", keys_tail->cmd);
            keys_tail->next = k;
            last = keys_tail;
            keys_tail = k;
            ++keys_count;
        }
        ++i;
    }

    if (keys_tail->cmd == NULL) {
        keys_tail = last;
        free(keys_tail->next);
        keys_tail->next = NULL;
    }

    switch (errno) {
    case EINVAL: /* FALLTHROUGH */
    case ENOMEM:
    case EOVERFLOW:
        cleanup();
        fclose(kfp);
        free(line);
        err("Failed to parse keys file: '%s'\nError in my_getline: '%i'",
            keysrc_file, errno);
        break;
    }

    fclose(kfp);
    free(line);
    /* return make_key_array(); */
    return 1;
}

int
parse_keys_line(char *str)
{
    PUTS("\nFunction: parse_keys_line");
    PRINTF("\tstr........'%s'\n", str);

    /* Test blank lines */
    size_t len = strlen(str);
    if (!len) {
        PUTS("\tSkipping blank line.");
        return 0;
    }

    /* Line is not blank, copy str into an array */
    char str_cpy[len + 1];
    char *s;
    strcpy(str_cpy, str);
    s = trim_whitespace(str_cpy);

    /* Skip comments */
    if ((*s) == comment_char) {
        PUTS("\tSkipping comment");
        return 0;
    } else if ((*s) == '\0') {
        PUTS("\tSkipping blank line.");
        return 0;
    }

    /* Found a line that isn't a comment or blank */
    /* Setup */
    int m_len = 0;
    char *start, *end, *curr;
    start = end = curr = s;

    while ((m_len = get_key(&start, &end))) {
        if (m_len < 0)
            return -1;
        curr = end;
        for (; (ISSPACE(curr) || *curr == '+') && m_len > 0; --m_len)
            --curr;
        unsigned int mod;
        mod = get_mod(start, m_len);
        if (!mod)
            return -2;
        keys_tail->mod |= mod;
        start = curr = ++end;
    }

    /* Got a key */
    curr = end;
    while ((ISSPACE(curr) || *curr == command_char) && curr != start)
        --curr;
    char key[(curr - start) + 1];
    sprintf(key, "%.*s", (int)(curr - start) + 1, start);
    KeySym ksym, lower, upper;
    ksym = XStringToKeysym(key);
    if ((ksym | NoSymbol) == NoSymbol) {
        ksym = xf86_str_to_keysym(key);
        if ((ksym | NoSymbol) == NoSymbol)
            return -3;
    } else {
        XConvertCase(ksym, &lower, &upper);
        ksym = lower;
    }
    keys_tail->key = ksym;

    /* Process cmd */
    while ((ISSPACE(end) || *end == command_char) && *end != '\0')
        ++end;
    if (*end == '\0')
        return -2;
    start = curr = end;
    end = clean_spaces(end);
    int c_len = end - start;
    char *cmd;
    cmd = malloc(sizeof(char) * (c_len + 1));
    if (cmd == NULL)
        return -1;
    strcpy(cmd, start);
    keys_tail->cmd = cmd;

    return 1;
}

int
get_key(char **start, char **end)
{
    int i;
    /* Strip leading whitespace */
    while (isspace((unsigned char)(**end)))
        (*end)++;
    *start = *end;
    for (i = 0; **end != '\0' && **end != '+' &&
                **end != command_char; ++i) {
        (*end)++;
    }
    switch (**end) {
    case '+':
        return i;
    case '=':
        return 0;
    case '\0':
        return -1;
    }
    return 0;
}

unsigned int
get_mod(char *start, int len)
{
    PUTS("\nFunction: get_mod");
    char mod[len + 1];
    sprintf(mod, "%.*s", len + 1, start);
    PRINTF("\tmod.......'%s'\n\n", mod);
    unsigned int ret;
    ret = 0;
    switch (len + 1) {
    case 3: /* alt */
        if (!strcmp(mod, "alt"))
            ret = Mod1Mask;
        break;
    case 4: /* ctrl, meta, mod{1..5}, lock */
        if (!strcmp(mod, "ctrl")) {
            ret = ControlMask;
        } else if (!strcmp(mod, "meta")) {
            ret = Mod1Mask;
        } else if (!strcmp(mod, "mod1")) {
            ret = Mod1Mask;
        } else if (!strcmp(mod, "mod2")) {
            ret = Mod2Mask;
        } else if (!strcmp(mod, "mod3")) {
            ret = Mod3Mask;
        } else if (!strcmp(mod, "mod4")) {
            ret = Mod4Mask;
        } else if (!strcmp(mod, "mod5")) {
            ret = Mod5Mask;
        }
        break;
    case 5: /* shift, super, hyper */
    case 7: /* control */
        if (!strcmp(mod, "shift")) {
            ret = ShiftMask;
        } else if (!strcmp(mod, "super")) {
            ret = Mod4Mask;
        } else if (!strcmp(mod, "hyper")) {
            ret = Mod4Mask;
        } else if (!strcmp(mod, "control")) {
            ret = ControlMask;
        }
        break;
    }
    return ret;
}

KeySym
xf86_str_to_keysym(char *str)
{
    int i;
    for (i = 0; i < xf86_len; ++i)
        if (!strcmp(kd[i].keyname, str))
            return kd[i].keysym;
    return 0L;
}

Keys *
make_keys(void)
{
    Keys *k = calloc(1, sizeof(Keys));
    if (k == NULL) {
        return k;
    }
    k->mod  = 0;
    k->key  = 0;
    k->cmd  = NULL;
    k->next = NULL;
    return k;
}

Key *
convert_keys_list(Keys *curr)
{
    Key *key_ptr, *k_curr;
    key_ptr = k_curr = NULL;

    k_curr = key_ptr = malloc(sizeof(Key) * (keys_count + 1));
    if (key_ptr == NULL) {
        cleanup();
        err("ERROR: Could not create keys array.\n");
    }
    if (append_to_free_list(key_ptr, free_keys_array) < 0) {
        cleanup();
        err("ERROR: Could not allocate memory for free list\n");
    }

    while (curr != NULL) {
        k_curr->mod = curr->mod;
        k_curr->key = curr->key;
        k_curr->cmd = curr->cmd;
        curr = curr->next;
        ++k_curr;
    }
    k_curr->mod = 0;
    k_curr->key = 0;
    k_curr->cmd = NULL;

    return key_ptr;
}

int
write_keys_file(const char *out_file)
{
    PUTS("\nFunction: write_binds_file");
    PRINTF("out_file: '%s'\n", out_file);
    FILE *ofp = NULL;
    if (out_file[0] == '\0')
        ofp = stdout;
    else
        ofp = fopen(out_file, "w");
    if (ofp == NULL)
        return -1;
    write_keys_header(ofp);
    if (write_keys_line(ofp, keys_head) < 0) {
        cleanup();
        err("ERROR: bad keys list");
    }
    write_keys_footer(ofp);
    fclose(ofp);
    PUTS(" ");
    return 1;
}

int
write_keys_header(FILE *__restrict fp)
{
    fputs(
        "/* See LICENSE file for copyright and license details. */\n"
        "#ifndef WKX_KEYS_H_\n"
        "#define WKX_KEYS_H_\n"
        "\n"
        "#include <X11/XF86keysym.h>\n"
        "\n"
        "const Key keys[] = {\n"
        "    /* mod(s)           key             cmd */\n", fp);
    return 1;
}

int
write_keys_line(FILE *__restrict fp, Keys *curr)
{
    char tmp[MAXLEN * 2];
    char *k_str;
    while (curr != NULL) {
        fprintf(fp, "%*c{", 4, ' ');
        x_mod_to_str(curr->mod, tmp);
        if (*tmp == '\0')
            return -1;
        fprintf(fp, " %s,\t\t", tmp);
        k_str = xf86_keysym_to_string(curr->key);
        if (k_str == NULL) {
            k_str = XKeysymToString(curr->key);
            fprintf(fp, "XK_%s,\t\t", k_str);
        } else {
            fprintf(fp, "%s,\t\t", k_str);
        }
        if (curr->next != NULL)
            fprintf(fp, "\"%s\"},\n", esc_str(tmp, curr->cmd));
        else
            fprintf(fp, "\"%s\"}\n", esc_str(tmp, curr->cmd));
        curr = curr->next;
    }
    return 1;
}

int
write_keys_footer(FILE *__restrict fp)
{
    fputs("};\n"
         "\n"
         "#endif /* WKX_KEYS_H_ */\n", fp);
    return 1;
}

void
x_mod_to_str(unsigned int mod, char *tmp)
{
    if (Flags & MULTI_FLAG)
        Flags ^= MULTI_FLAG;
    *tmp = '\0';
    if (mod & ShiftMask) {
        strcat(tmp, "ShiftMask");
        Flags |= MULTI_FLAG;
    }
    if (mod & ControlMask && Flags & MULTI_FLAG) {
        strcat(tmp, "|ControlMask");
    } else if (mod & ControlMask) {
        strcat(tmp, "ControlMask");
        Flags |= MULTI_FLAG;
    }
    if (mod & Mod1Mask && Flags & MULTI_FLAG) {
        strcat(tmp, "|Mod1Mask");
    } else if (mod & Mod1Mask) {
        strcat(tmp, "Mod1Mask");
        Flags |= MULTI_FLAG;
    }
    if (mod & Mod2Mask && Flags & MULTI_FLAG) {
        strcat(tmp, "|Mod2Mask");
    } else if (mod & Mod2Mask) {
        strcat(tmp, "Mod2Mask");
        Flags |= MULTI_FLAG;
    }
    if (mod & Mod3Mask && Flags & MULTI_FLAG) {
        strcat(tmp, "|Mod3Mask");
    } else if (mod & Mod3Mask) {
        strcat(tmp, "Mod3Mask");
        Flags |= MULTI_FLAG;
    }
    if (mod & Mod4Mask && Flags & MULTI_FLAG) {
        strcat(tmp, "|Mod4Mask");
    } else if (mod & Mod4Mask) {
        strcat(tmp, "Mod4Mask");
        Flags |= MULTI_FLAG;
    }
    if (mod & Mod5Mask && Flags & MULTI_FLAG) {
        strcat(tmp, "|Mod5Mask");
    } else if (mod & Mod5Mask) {
        strcat(tmp, "Mod5Mask");
        Flags |= MULTI_FLAG;
    }
    if (mod & LockMask && Flags & MULTI_FLAG) {
        strcat(tmp, "|LockMask");
    } else if (mod & LockMask) {
        strcat(tmp, "LockMask");
        Flags |= MULTI_FLAG;
    }
    if (*tmp == '\0') {
        strcat(tmp, "0");
    }
}

char *
xf86_keysym_to_string(KeySym ksym)
{
    int i;
    for (i = 0; i < xf86_len; ++i)
        if (ksym == kd[i].keysym)
            return (char *)kd[i].p_name;
    return NULL;
}

/**************
 * misc funcs *
 **************/
char *
trim_whitespace(char *str)
{
    char *end;

    /* Trim leading space */
    for (; isspace((unsigned char)*str); ++str) ;

    if(*str == 0)  /* All spaces? */
        return str;

    /* Trim trailing space */
    end = str + strlen(str) - 1;
    for (; end > str && isspace((unsigned char)*end); --end) ;

    /* Write new null terminator character */
    end[1] = '\0';

    return str;
}

char *
clean_spaces(char *str)
{
    char *curr, *end, *sloop, *eloop;
    for (curr = str; *curr != '\0'; ++curr) {
        end = curr;
        if (ISSPACE(end++)) {
            while (ISSPACE(end))
                ++end;
            if (end - curr > 1) {
                sloop = curr;
                eloop = end;
                for (++sloop; *eloop != '\0'; ++sloop, ++eloop) {
                    *sloop = *eloop;
                }
                *sloop = *eloop;
            }
        }
    }
    return curr;
}

void
define_output_file(void)
{
    PUTS("\nFunction: define_output_file");
    if (output_path != NULL) {
        if (output_path[0] != '/') {
            PRINTF("\toutput_path looks relative: %s\nPreappending cwd\n", output_path);
            if (getcwd(cwd, CWD_SIZE) == NULL) {
                cleanup();
                err("\nError getting cwd...exiting.\n");
            }
            PRINTF("\tcwd detected as %s\n", cwd);
            snprintf(output_file, FILE_SIZE, "%s/%s", cwd, output_path);
        } else {
            snprintf(output_file, FILE_SIZE, "%s", output_path);
        }
    }
    PRINTF("\toutput_path = %s\n", output_path);
    PRINTF("\toutput_file = %s\n", output_file);
}

char *
esc_str(char tmp[], char *str)
{
    int i = 0;
    tmp[i] = '\0';
    while (*str != '\0' && i < (MAXLEN * 2) - 1) {
        if (ESC_TEST(*str))
            tmp[i++] = '\\';
        tmp[i++] = *str++;
    }
    tmp[i] = '\0';
    return tmp;
}

FreeList *
make_free_list(void)
{
    FreeList *f = calloc(1, sizeof(FreeList));
    if (f == NULL) {
        return f;
    }
    f->next = NULL;
    f->fp   = NULL;
    f->item = NULL;
    return f;
}

int
append_to_free_list(void *item, void (*fp)(void *))
{
    if (free_tail->item == NULL) {
        free_tail->item = item;
        free_tail->fp   = fp;
        free_tail->next = NULL;
    } else {
        FreeList *f;
        if (!(f = make_free_list())) {
            return -1;
        }
        f->item         = item;
        f->fp           = fp;
        f->next         = NULL;
        free_tail->next = f;
        free_tail       = f;
    }
    return 0;
}

int64_t
my_getline(char **__restrict l, size_t *__restrict len, FILE *__restrict fp)
{
    /* Check if either line, len or fp are NULL pointers */
    if (l == NULL || len == NULL || fp == NULL) {
        errno = EINVAL;
        return -1;
    }

    /* Use a chunk array of 128 bytes as parameter for fgets */
    char chunk[MAXLEN];

    /* Allocate a block of memory for *line if it is NULL or smaller than the chunk array */
    if (*l == NULL || *len < sizeof(chunk)) {
        *len = sizeof(chunk);
        if ((*l = malloc(*len)) == NULL) {
            errno = ENOMEM;
            return -1;
        }
    }

    /* "Empty" the string */
    (*l)[0] = '\0';

    while (fgets(chunk, sizeof(chunk), fp) != NULL) {
        /* Resize the line buffer if necessary */
        size_t len_used = strlen(*l);
        size_t chunk_used = strlen(chunk);

        if (*len - len_used < chunk_used) {
            /* Check for overflow */
            if (*len > SIZE_MAX / 2) {
                errno = EOVERFLOW;
                return -1;
            } else {
                *len *= 2;
            }

            if ((*l = realloc(*l, *len)) == NULL) {
                errno = ENOMEM;
                return -1;
            }
        }

        /* Copy the chunk to the end of the line buffer */
        memcpy(*l + len_used, chunk, chunk_used);
        len_used += chunk_used;
        (*l)[len_used] = '\0';

        /* Don't want to overstep our bounds */
        size_t n;
        (len_used > 1) ? (n = 2) : (n = 0);

        /* Check if *line contains '\n',
         * if yes, check if escape is used
         *     if yes, continue reading.
         *     if no, return *line len
         */
        if ((*l)[len_used - 1] == '\n') {
            if ((*l)[len_used - n] == '\\') {
                (*l)[len_used - 1] = ' ';
                (*l)[len_used - n] = ' ';
            } else {
                (*l)[len_used - 1] = '\0';
                return len_used - 1;
            }
        }
    }

    return -1;
}

