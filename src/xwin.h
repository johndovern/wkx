/* See LICENSE file for copyright and license details. */
#ifndef WKX_XWIN_H
#define WKX_XWIN_H

#include <X11/Xlib.h>

#include "types.h"

enum { SchemeNorm, SchemeLast }; /* color schemes */

unsigned int calc_lines(unsigned int i);
void calc_cols(unsigned int i);
unsigned int calc_items(Chord *curr);
int drawitem(Chord *curr, int x, int y, int w);
void drawmenu(Chord *curr, unsigned int items, unsigned int lines);
void keypress(XKeyEvent *ev);
int process_keypress(char *buf, unsigned int mod_flag);
int specialkey(char *buf, KeySym ksym, unsigned int state);
void grabfocus(void);
void grabkeyboard(void);
void run(void);
void setup(void);
int xwin(Chord *curr);
void x_cleanup(void);

#endif // WKX_XWIN_H
