/* See LICENSE file for copyright and license details. */
#include <errno.h>
#include <fcntl.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>

#include "helpers.h"
#include "wkx.h"

void
warn(char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
}

void
err(char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    exit(EXIT_FAILURE);
}

int
press_key(char *keys)
{
    PUTS("Function: press_key");
    Chord *curr;
    char *k;
    char key[32] = { '\0' };
    int i;
    k = keys;
    i = 0;
    LSTRIP(k); /* Strip leading white space */
    while (*k != '\0') {
        if (!ISSPACE(k)) {
            key[i++] = *k;
        } else {
            key[i] = '\0';
            i = 0;
            PRINTF("\tkey......'%s'\n", key);
            if ((curr = find_key(key, chords_ptr)) == NULL)
                return -1;
            if (curr->chords == NULL)
                return -1;
            else
                chords_ptr = curr->chords;
        }
        ++k;
    }
    key[i] = '\0';
    i = 0;
    PUTS("\tFinal key");
    PRINTF("\tkey......'%s'\n", key);
    if ((curr = find_key(key, chords_ptr)) == NULL)
        return -1;
    if (curr->chords == NULL && curr->cmd != NULL) {
        spawn(curr->cmd);
        return 0;
    } else {
        chords_ptr = curr->chords;
    }
    return 1;
}

Chord *
find_key(const char *buf, Chord *curr)
{
    PUTS("\nFunction: find_key");
    PRINTF("\tbuf.......'%s'\n", buf);
    while (curr->key != NULL) {
        if (!strcmp(curr->key, buf)) {
            PRINTF("\tPressing..'%s'\n", curr->key);
            return curr;
        }
        ++curr;
    }
    return NULL;
}

void
daemon_spawn(char *cmd, Display *dpy)
{
    if (fork() == 0) {
        if (dpy != NULL)
            close(ConnectionNumber(dpy));
        spawn(cmd);
        cleanup();
        exit(EXIT_SUCCESS);
    }
    wait(NULL);
}

void
spawn(char *cmd)
{
    if (fork() == 0) {
        setsid();
        if (redir_fd != -1) {
            dup2(redir_fd, STDOUT_FILENO);
            dup2(redir_fd, STDERR_FILENO);
        }
        char *exec[] = { shell, "-c", cmd, NULL };
        execvp(exec[0], exec);
        cleanup();
        err("ERROR: execvp '%s' failed\n",
            cmd[0]);
    }
}

void
define_redir_file(const char def[],
                  const char *path,
                  char file[],
                  const int trunc_flag)
{
    if (path == NULL && def != NULL && def[0] != '\0') {
        if (*def != '/') {
            define_tmp_file(def, file);
        } else {
            snprintf(file, FILE_SIZE, "%s", def);
        }
    } else if (path != NULL) {
        if (path[0] != '/') {
            define_tmp_file(path, file);
        } else {
            snprintf(file, FILE_SIZE, "%s", path);
        }
    }
    if (file[0] != '\0') {
        if (trunc_flag)
            redir_fd = open(file, O_WRONLY|O_CREAT|O_TRUNC,
                            S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
        else
            redir_fd = open(file, O_WRONLY|O_CREAT,
                            S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
        if (redir_fd < 0)
            warn("WARNING: Failed to open redirection file: '%s'\n",
                 file);
    }
    PRINTF("%s......'%i'\n", def, redir_fd);
}

void
define_tmp_file(const char *file, char *bucket)
{
    char *tmp = getenv(TMP_ENV);
    if (tmp == NULL) {
        warn("WARNING: Could not get $TMPDIR environment var. Defaulting to '%s'\n", TMP_DIR);
        tmp = TMP_DIR;
    }
    snprintf(bucket, FILE_SIZE, "%s/wkx", tmp);
    struct stat s;
    int e = stat(bucket, &s);
    if (e < 0 && errno == ENOENT) {
        if (mkdir(bucket, 0700)) {
            warn("WARNING: Could not create tmp dir: '%s'\n", bucket);
        }
    } else if (e < 0) {
        perror("stat");
        exit(EXIT_FAILURE);
    }
    strncat(bucket, "/", FILE_SIZE);
    strncat(bucket, file, FILE_SIZE);
    PRINTF("tmp file........'%s'\n", bucket);
}
