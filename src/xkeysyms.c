/* See LICENSE file for copyright and license details. */
#include "helpers.h"
#include "xkeysyms.h"

const KeysDict kd[] = {
    { "XF86XK_ModeLock"                 , "XF86ModeLock"                 ,  0x1008FF01 },        /* Mode Switch Lock */
/* Backlight controls. */
    { "XF86XK_MonBrightnessUp"          , "XF86MonBrightnessUp"          ,  0x1008FF02 },  /* Monitor/panel brightness */
    { "XF86XK_MonBrightnessDown"        , "XF86MonBrightnessDown"        ,  0x1008FF03 },  /* Monitor/panel brightness */
    { "XF86XK_KbdLightOnOff"            , "XF86KbdLightOnOff"            ,  0x1008FF04 },  /* Keyboards may be lit     */
    { "XF86XK_KbdBrightnessUp"          , "XF86KbdBrightnessUp"          ,  0x1008FF05 },  /* Keyboards may be lit     */
    { "XF86XK_KbdBrightnessDown"        , "XF86KbdBrightnessDown"        ,  0x1008FF06 },  /* Keyboards may be lit     */
/*
* Keys found on some "Internet" keyboards.
*/
    { "XF86XK_Standby"                  , "XF86Standby"                  ,  0x1008FF10 },   /* System into standby mode   */
    { "XF86XK_AudioLowerVolume"         , "XF86AudioLowerVolume"         ,  0x1008FF11 },   /* Volume control down        */
    { "XF86XK_AudioMute"                , "XF86AudioMute"                ,  0x1008FF12 },   /* Mute sound from the system */
    { "XF86XK_AudioRaiseVolume"         , "XF86AudioRaiseVolume"         ,  0x1008FF13 },   /* Volume control up          */
    { "XF86XK_AudioPlay"                , "XF86AudioPlay"                ,  0x1008FF14 },   /* Start playing of audio >   */
    { "XF86XK_AudioStop"                , "XF86AudioStop"                ,  0x1008FF15 },   /* Stop playing audio         */
    { "XF86XK_AudioPrev"                , "XF86AudioPrev"                ,  0x1008FF16 },   /* Previous track             */
    { "XF86XK_AudioNext"                , "XF86AudioNext"                ,  0x1008FF17 },   /* Next track                 */
    { "XF86XK_HomePage"                 , "XF86HomePage"                 ,  0x1008FF18 },   /* Display user's home page   */
    { "XF86XK_Mail"                     , "XF86Mail"                     ,  0x1008FF19 },   /* Invoke user's mail program */
    { "XF86XK_Start"                    , "XF86Start"                    ,  0x1008FF1A },   /* Start application          */
    { "XF86XK_Search"                   , "XF86Search"                   ,  0x1008FF1B },   /* Search                     */
    { "XF86XK_AudioRecord"              , "XF86AudioRecord"              ,  0x1008FF1C },   /* Record audio application   */
/* These are sometimes found on PDA's (e.g. Palm, PocketPC or elsewhere)   */
    { "XF86XK_Calculator"               , "XF86Calculator"               ,  0x1008FF1D },   /* Invoke calculator program  */
    { "XF86XK_Memo"                     , "XF86Memo"                     ,  0x1008FF1E },   /* Invoke Memo taking program */
    { "XF86XK_ToDoList"                 , "XF86ToDoList"                 ,  0x1008FF1F },   /* Invoke To Do List program  */
    { "XF86XK_Calendar"                 , "XF86Calendar"                 ,  0x1008FF20 },   /* Invoke Calendar program    */
    { "XF86XK_PowerDown"                , "XF86PowerDown"                ,  0x1008FF21 },   /* Deep sleep the system      */
    { "XF86XK_ContrastAdjust"           , "XF86ContrastAdjust"           ,  0x1008FF22 },   /* Adjust screen contrast     */
    { "XF86XK_RockerUp"                 , "XF86RockerUp"                 ,  0x1008FF23 },   /* Rocker switches exist up   */
    { "XF86XK_RockerDown"               , "XF86RockerDown"               ,  0x1008FF24 },   /* and down                   */
    { "XF86XK_RockerEnter"              , "XF86RockerEnter"              ,  0x1008FF25 },   /* and let you press them     */
/* Some more "Internet" keyboard symbols */
    { "XF86XK_Back"                     , "XF86Back"                     ,  0x1008FF26 },   /* Like back on a browser     */
    { "XF86XK_Forward"                  , "XF86Forward"                  ,  0x1008FF27 },   /* Like forward on a browser  */
    { "XF86XK_Stop"                     , "XF86Stop"                     ,  0x1008FF28 },   /* Stop current operation     */
    { "XF86XK_Refresh"                  , "XF86Refresh"                  ,  0x1008FF29 },   /* Refresh the page           */
    { "XF86XK_PowerOff"                 , "XF86PowerOff"                 ,  0x1008FF2A },   /* Power off system entirely  */
    { "XF86XK_WakeUp"                   , "XF86WakeUp"                   ,  0x1008FF2B },   /* Wake up system from sleep  */
    { "XF86XK_Eject"                    , "XF86Eject"                    ,  0x1008FF2C },   /* Eject device (e.g. DVD)    */
    { "XF86XK_ScreenSaver"              , "XF86ScreenSaver"              ,  0x1008FF2D },   /* Invoke screensaver         */
    { "XF86XK_WWW"                      , "XF86WWW"                      ,  0x1008FF2E },   /* Invoke web browser         */
    { "XF86XK_Sleep"                    , "XF86Sleep"                    ,  0x1008FF2F },   /* Put system to sleep        */
    { "XF86XK_Favorites"                , "XF86Favorites"                ,  0x1008FF30 },   /* Show favorite locations    */
    { "XF86XK_AudioPause"               , "XF86AudioPause"               ,  0x1008FF31 },   /* Pause audio playing        */
    { "XF86XK_AudioMedia"               , "XF86AudioMedia"               ,  0x1008FF32 },   /* Launch media collection app */
    { "XF86XK_MyComputer"               , "XF86MyComputer"               ,  0x1008FF33 },   /* Display "My Computer" window */
    { "XF86XK_VendorHome"               , "XF86VendorHome"               ,  0x1008FF34 },   /* Display vendor home web site */
    { "XF86XK_LightBulb"                , "XF86LightBulb"                ,  0x1008FF35 },   /* Light bulb keys exist       */
    { "XF86XK_Shop"                     , "XF86Shop"                     ,  0x1008FF36 },   /* Display shopping web site   */
    { "XF86XK_History"                  , "XF86History"                  ,  0x1008FF37 },   /* Show history of web surfing */
    { "XF86XK_OpenURL"                  , "XF86OpenURL"                  ,  0x1008FF38 },   /* Open selected URL           */
    { "XF86XK_AddFavorite"              , "XF86AddFavorite"              ,  0x1008FF39 },   /* Add URL to favorites list   */
    { "XF86XK_HotLinks"                 , "XF86HotLinks"                 ,  0x1008FF3A },   /* Show "hot" links            */
    { "XF86XK_BrightnessAdjust"         , "XF86BrightnessAdjust"         ,  0x1008FF3B },   /* Invoke brightness adj. UI   */
    { "XF86XK_Finance"                  , "XF86Finance"                  ,  0x1008FF3C },   /* Display financial site      */
    { "XF86XK_Community"                , "XF86Community"                ,  0x1008FF3D },   /* Display user's community    */
    { "XF86XK_AudioRewind"              , "XF86AudioRewind"              ,  0x1008FF3E },   /* "rewind" audio track        */
    { "XF86XK_BackForward"              , "XF86BackForward"              ,  0x1008FF3F },   /* ??? */
    { "XF86XK_Launch0"                  , "XF86Launch0"                  ,  0x1008FF40 },   /* Launch Application          */
    { "XF86XK_Launch1"                  , "XF86Launch1"                  ,  0x1008FF41 },   /* Launch Application          */
    { "XF86XK_Launch2"                  , "XF86Launch2"                  ,  0x1008FF42 },   /* Launch Application          */
    { "XF86XK_Launch3"                  , "XF86Launch3"                  ,  0x1008FF43 },   /* Launch Application          */
    { "XF86XK_Launch4"                  , "XF86Launch4"                  ,  0x1008FF44 },   /* Launch Application          */
    { "XF86XK_Launch5"                  , "XF86Launch5"                  ,  0x1008FF45 },   /* Launch Application          */
    { "XF86XK_Launch6"                  , "XF86Launch6"                  ,  0x1008FF46 },   /* Launch Application          */
    { "XF86XK_Launch7"                  , "XF86Launch7"                  ,  0x1008FF47 },   /* Launch Application          */
    { "XF86XK_Launch8"                  , "XF86Launch8"                  ,  0x1008FF48 },   /* Launch Application          */
    { "XF86XK_Launch9"                  , "XF86Launch9"                  ,  0x1008FF49 },   /* Launch Application          */
    { "XF86XK_LaunchA"                  , "XF86LaunchA"                  ,  0x1008FF4A },   /* Launch Application          */
    { "XF86XK_LaunchB"                  , "XF86LaunchB"                  ,  0x1008FF4B },   /* Launch Application          */
    { "XF86XK_LaunchC"                  , "XF86LaunchC"                  ,  0x1008FF4C },   /* Launch Application          */
    { "XF86XK_LaunchD"                  , "XF86LaunchD"                  ,  0x1008FF4D },   /* Launch Application          */
    { "XF86XK_LaunchE"                  , "XF86LaunchE"                  ,  0x1008FF4E },   /* Launch Application          */
    { "XF86XK_LaunchF"                  , "XF86LaunchF"                  ,  0x1008FF4F },   /* Launch Application          */
    { "XF86XK_ApplicationLeft"          , "XF86ApplicationLeft"          ,  0x1008FF50 },   /* switch to application, left */
    { "XF86XK_ApplicationRight"         , "XF86ApplicationRight"         ,  0x1008FF51 },   /* switch to application, right*/
    { "XF86XK_Book"                     , "XF86Book"                     ,  0x1008FF52 },   /* Launch bookreader           */
    { "XF86XK_CD"                       , "XF86CD"                       ,  0x1008FF53 },   /* Launch CD/DVD player        */
    { "XF86XK_Calculater"               , "XF86Calculater"               ,  0x1008FF54 },   /* Launch Calculater           */
    { "XF86XK_Clear"                    , "XF86Clear"                    ,  0x1008FF55 },   /* Clear window, screen        */
    { "XF86XK_Close"                    , "XF86Close"                    ,  0x1008FF56 },   /* Close window                */
    { "XF86XK_Copy"                     , "XF86Copy"                     ,  0x1008FF57 },   /* Copy selection              */
    { "XF86XK_Cut"                      , "XF86Cut"                      ,  0x1008FF58 },   /* Cut selection               */
    { "XF86XK_Display"                  , "XF86Display"                  ,  0x1008FF59 },   /* Output switch key           */
    { "XF86XK_DOS"                      , "XF86DOS"                      ,  0x1008FF5A },   /* Launch DOS (emulation)      */
    { "XF86XK_Documents"                , "XF86Documents"                ,  0x1008FF5B },   /* Open documents window       */
    { "XF86XK_Excel"                    , "XF86Excel"                    ,  0x1008FF5C },   /* Launch spread sheet         */
    { "XF86XK_Explorer"                 , "XF86Explorer"                 ,  0x1008FF5D },   /* Launch file explorer        */
    { "XF86XK_Game"                     , "XF86Game"                     ,  0x1008FF5E },   /* Launch game                 */
    { "XF86XK_Go"                       , "XF86Go"                       ,  0x1008FF5F },   /* Go to URL                   */
    { "XF86XK_iTouch"                   , "XF86iTouch"                   ,  0x1008FF60 },   /* Logitch iTouch- don't use   */
    { "XF86XK_LogOff"                   , "XF86LogOff"                   ,  0x1008FF61 },   /* Log off system              */
    { "XF86XK_Market"                   , "XF86Market"                   ,  0x1008FF62 },   /* ??                          */
    { "XF86XK_Meeting"                  , "XF86Meeting"                  ,  0x1008FF63 },   /* enter meeting in calendar   */
    { "XF86XK_MenuKB"                   , "XF86MenuKB"                   ,  0x1008FF65 },   /* distingush keyboard from PB */
    { "XF86XK_MenuPB"                   , "XF86MenuPB"                   ,  0x1008FF66 },   /* distinuish PB from keyboard */
    { "XF86XK_MySites"                  , "XF86MySites"                  ,  0x1008FF67 },   /* Favourites                  */
    { "XF86XK_New"                      , "XF86New"                      ,  0x1008FF68 },   /* New (folder, document...    */
    { "XF86XK_News"                     , "XF86News"                     ,  0x1008FF69 },   /* News                        */
    { "XF86XK_OfficeHome"               , "XF86OfficeHome"               ,  0x1008FF6A },   /* Office home (old Staroffice)*/
    { "XF86XK_Open"                     , "XF86Open"                     ,  0x1008FF6B },   /* Open                        */
    { "XF86XK_Option"                   , "XF86Option"                   ,  0x1008FF6C },   /* ?? */
    { "XF86XK_Paste"                    , "XF86Paste"                    ,  0x1008FF6D },   /* Paste                       */
    { "XF86XK_Phone"                    , "XF86Phone"                    ,  0x1008FF6E },   /* Launch phone; dial number   */
    { "XF86XK_Q"                        , "XF86Q"                        ,  0x1008FF70 },   /* Compaq's Q - don't use      */
    { "XF86XK_Reply"                    , "XF86Reply"                    ,  0x1008FF72 },   /* Reply e.g., mail            */
    { "XF86XK_Reload"                   , "XF86Reload"                   ,  0x1008FF73 },   /* Reload web page, file, etc. */
    { "XF86XK_RotateWindows"            , "XF86RotateWindows"            ,  0x1008FF74 },   /* Rotate windows e.g. xrandr  */
    { "XF86XK_RotationPB"               , "XF86RotationPB"               ,  0x1008FF75 },   /* don't use                   */
    { "XF86XK_RotationKB"               , "XF86RotationKB"               ,  0x1008FF76 },   /* don't use                   */
    { "XF86XK_Save"                     , "XF86Save"                     ,  0x1008FF77 },   /* Save (file, document, state */
    { "XF86XK_ScrollUp"                 , "XF86ScrollUp"                 ,  0x1008FF78 },   /* Scroll window/contents up   */
    { "XF86XK_ScrollDown"               , "XF86ScrollDown"               ,  0x1008FF79 },   /* Scrool window/contentd down */
    { "XF86XK_ScrollClick"              , "XF86ScrollClick"              ,  0x1008FF7A },   /* Use XKB mousekeys instead   */
    { "XF86XK_Send"                     , "XF86Send"                     ,  0x1008FF7B },   /* Send mail, file, object     */
    { "XF86XK_Spell"                    , "XF86Spell"                    ,  0x1008FF7C },   /* Spell checker               */
    { "XF86XK_SplitScreen"              , "XF86SplitScreen"              ,  0x1008FF7D },   /* Split window or screen      */
    { "XF86XK_Support"                  , "XF86Support"                  ,  0x1008FF7E },   /* Get support (??)            */
    { "XF86XK_TaskPane"                 , "XF86TaskPane"                 ,  0x1008FF7F },   /* Show tasks */
    { "XF86XK_Terminal"                 , "XF86Terminal"                 ,  0x1008FF80 },   /* Launch terminal emulator    */
    { "XF86XK_Tools"                    , "XF86Tools"                    ,  0x1008FF81 },   /* toolbox of desktop/app.     */
    { "XF86XK_Travel"                   , "XF86Travel"                   ,  0x1008FF82 },   /* ?? */
    { "XF86XK_UserPB"                   , "XF86UserPB"                   ,  0x1008FF84 },   /* ?? */
    { "XF86XK_User1KB"                  , "XF86User1KB"                  ,  0x1008FF85 },   /* ?? */
    { "XF86XK_User2KB"                  , "XF86User2KB"                  ,  0x1008FF86 },   /* ?? */
    { "XF86XK_Video"                    , "XF86Video"                    ,  0x1008FF87 },   /* Launch video player       */
    { "XF86XK_WheelButton"              , "XF86WheelButton"              ,  0x1008FF88 },   /* button from a mouse wheel */
    { "XF86XK_Word"                     , "XF86Word"                     ,  0x1008FF89 },   /* Launch word processor     */
    { "XF86XK_Xfer"                     , "XF86Xfer"                     ,  0x1008FF8A },
    { "XF86XK_ZoomIn"                   , "XF86ZoomIn"                   ,  0x1008FF8B },   /* zoom in view, map, etc.   */
    { "XF86XK_ZoomOut"                  , "XF86ZoomOut"                  ,  0x1008FF8C },   /* zoom out view, map, etc.  */
    { "XF86XK_Away"                     , "XF86Away"                     ,  0x1008FF8D },   /* mark yourself as away     */
    { "XF86XK_Messenger"                , "XF86Messenger"                ,  0x1008FF8E },   /* as in instant messaging   */
    { "XF86XK_WebCam"                   , "XF86WebCam"                   ,  0x1008FF8F },   /* Launch web camera app.    */
    { "XF86XK_MailForward"              , "XF86MailForward"              ,  0x1008FF90 },   /* Forward in mail           */
    { "XF86XK_Pictures"                 , "XF86Pictures"                 ,  0x1008FF91 },   /* Show pictures             */
    { "XF86XK_Music"                    , "XF86Music"                    ,  0x1008FF92 },   /* Launch music application  */
    { "XF86XK_Battery"                  , "XF86Battery"                  ,  0x1008FF93 },   /* Display battery information */
    { "XF86XK_Bluetooth"                , "XF86Bluetooth"                ,  0x1008FF94 },   /* Enable/disable Bluetooth    */
    { "XF86XK_WLAN"                     , "XF86WLAN"                     ,  0x1008FF95 },   /* Enable/disable WLAN         */
    { "XF86XK_UWB"                      , "XF86UWB"                      ,  0x1008FF96 },   /* Enable/disable UWB            */
    { "XF86XK_AudioForward"             , "XF86AudioForward"             ,  0x1008FF97 },   /* fast-forward audio track    */
    { "XF86XK_AudioRepeat"              , "XF86AudioRepeat"              ,  0x1008FF98 },   /* toggle repeat mode          */
    { "XF86XK_AudioRandomPlay"          , "XF86AudioRandomPlay"          ,  0x1008FF99 },   /* toggle shuffle mode         */
    { "XF86XK_Subtitle"                 , "XF86Subtitle"                 ,  0x1008FF9A },   /* cycle through subtitle      */
    { "XF86XK_AudioCycleTrack"          , "XF86AudioCycleTrack"          ,  0x1008FF9B },   /* cycle through audio tracks  */
    { "XF86XK_CycleAngle"               , "XF86CycleAngle"               ,  0x1008FF9C },   /* cycle through angles        */
    { "XF86XK_FrameBack"                , "XF86FrameBack"                ,  0x1008FF9D },   /* video: go one frame back    */
    { "XF86XK_FrameForward"             , "XF86FrameForward"             ,  0x1008FF9E },   /* video: go one frame forward */
    { "XF86XK_Time"                     , "XF86Time"                     ,  0x1008FF9F },   /* display, or shows an entry for time seeking */
    { "XF86XK_Select"                   , "XF86Select"                   ,  0x1008FFA0 },   /* Select button on joypads and remotes */
    { "XF86XK_View"                     , "XF86View"                     ,  0x1008FFA1 },   /* Show a view options/properties */
    { "XF86XK_TopMenu"                  , "XF86TopMenu"                  ,  0x1008FFA2 },   /* Go to a top-level menu in a video */
    { "XF86XK_Red"                      , "XF86Red"                      ,  0x1008FFA3 },   /* Red button                  */
    { "XF86XK_Green"                    , "XF86Green"                    ,  0x1008FFA4 },   /* Green button                */
    { "XF86XK_Yellow"                   , "XF86Yellow"                   ,  0x1008FFA5 },   /* Yellow button               */
    { "XF86XK_Blue"                     , "XF86Blue"                     ,  0x1008FFA6 },   /* Blue button                 */
    { "XF86XK_Suspend"                  , "XF86Suspend"                  ,  0x1008FFA7 },   /* Sleep to RAM                */
    { "XF86XK_Hibernate"                , "XF86Hibernate"                ,  0x1008FFA8 },   /* Sleep to disk               */
    { "XF86XK_TouchpadToggle"           , "XF86TouchpadToggle"           ,  0x1008FFA9 },   /* Toggle between touchpad/trackstick */
    { "XF86XK_TouchpadOn"               , "XF86TouchpadOn"               ,  0x1008FFB0 },   /* The touchpad got switched on */
    { "XF86XK_TouchpadOff"              , "XF86TouchpadOff"              ,  0x1008FFB1 },   /* The touchpad got switched off */
    { "XF86XK_AudioMicMute"             , "XF86AudioMicMute"             ,  0x1008FFB2 },   /* Mute the Mic from the system */
    { "XF86XK_Keyboard"                 , "XF86Keyboard"                 ,  0x1008FFB3 },   /* User defined keyboard related action */
    { "XF86XK_WWAN"                     , "XF86WWAN"                     ,  0x1008FFB4 },   /* Toggle WWAN (LTE, UMTS, etc.) radio */
    { "XF86XK_RFKill"                   , "XF86RFKill"                   ,  0x1008FFB5 },   /* Toggle radios on/off */
    { "XF86XK_AudioPreset"              , "XF86AudioPreset"              ,  0x1008FFB6 },   /* Select equalizer preset, e.g. theatre-mode */
/* Keys for special action keys (hot keys) */
/* Virtual terminals on some operating systems */
    { "XF86XK_Switch_VT_1"              , "XF86Switch_VT_1"              ,  0x1008FE01 },
    { "XF86XK_Switch_VT_2"              , "XF86Switch_VT_2"              ,  0x1008FE02 },
    { "XF86XK_Switch_VT_3"              , "XF86Switch_VT_3"              ,  0x1008FE03 },
    { "XF86XK_Switch_VT_4"              , "XF86Switch_VT_4"              ,  0x1008FE04 },
    { "XF86XK_Switch_VT_5"              , "XF86Switch_VT_5"              ,  0x1008FE05 },
    { "XF86XK_Switch_VT_6"              , "XF86Switch_VT_6"              ,  0x1008FE06 },
    { "XF86XK_Switch_VT_7"              , "XF86Switch_VT_7"              ,  0x1008FE07 },
    { "XF86XK_Switch_VT_8"              , "XF86Switch_VT_8"              ,  0x1008FE08 },
    { "XF86XK_Switch_VT_9"              , "XF86Switch_VT_9"              ,  0x1008FE09 },
    { "XF86XK_Switch_VT_10"             , "XF86Switch_VT_10"             ,  0x1008FE0A },
    { "XF86XK_Switch_VT_11"             , "XF86Switch_VT_11"             ,  0x1008FE0B },
    { "XF86XK_Switch_VT_12"             , "XF86Switch_VT_12"             ,  0x1008FE0C },
    { "XF86XK_Ungrab"                   , "XF86Ungrab"                   ,  0x1008FE20 },   /* force ungrab               */
    { "XF86XK_ClearGrab"                , "XF86ClearGrab"                ,  0x1008FE21 },   /* kill application with grab */
    { "XF86XK_Next_VMode"               , "XF86Next_VMode"               ,  0x1008FE22 },   /* next video mode available  */
    { "XF86XK_Prev_VMode"               , "XF86Prev_VMode"               ,  0x1008FE23 },   /* prev. video mode available */
    { "XF86XK_LogWindowTree"            , "XF86LogWindowTree"            ,  0x1008FE24 },   /* print window tree to log   */
    { "XF86XK_LogGrabInfo"              , "XF86LogGrabInfo"              ,  0x1008FE25 },   /* print all active grabs to log */
};
const size_t xf86_len = LENGTH(kd);
