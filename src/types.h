/* See LICENSE file for copyright and license details. */
#ifndef WKX_TYPES_H
#define WKX_TYPES_H

#include <X11/Xlib.h>

typedef struct Bind Bind;
struct Bind {
    int   count;
    int   level;
    char *key;
    char *desc;
    char *cmd;
    Bind *prev;
    Bind *next;
};

typedef struct Chord Chord;
struct Chord {
    char  *key;
    char  *desc;
    char  *cmd;
    Chord *chords;
};

typedef struct FontList FontList;
struct FontList {
    char     *font;
    FontList *next;
};

typedef struct FreeList FreeList;
struct FreeList {
    void *item;
    void (*fp)(void *);
    FreeList *next;
};

typedef struct Key Key;
struct Key {
    unsigned int mod;
    KeySym       key;
    char        *cmd;
};

typedef struct Keys Keys;
struct Keys {
    unsigned int mod;
    KeySym       key;
    char        *cmd;
    Keys        *next;
};

typedef union Fp Fp;
union Fp {
    int (*c)(char *, const char *);
    int (*i)(int *, const char *);
    int (*s)(char **, const char *);
};

typedef struct Opts Opts;
struct Opts {
    char *opt;
    void *var;
    int ptr_val;
    Fp fp;
};

#endif /* WKX_TYPES_H */
