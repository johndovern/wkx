/* See LICENSE file for copyright and license details. */
#include <fcntl.h>
#include <getopt.h>
#include <linux/limits.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include <X11/Xft/Xft.h>

#include "drw.h"
#include "helpers.h"
#include "parser.h"
#include "types.h"
#include "util.h"
#include "wkx.h"
#include "xdaemon.h"
#include "xwin.h"

#include "binds.h"
#include "config.h"
#include "keys.h"

/* Files/paths */
char binds_file[PATH_MAX];
char *binds_path;
char config_file[PATH_MAX];
char *config_path;
char keys_file[PATH_MAX];
char *keys_path;
char redir_file[PATH_MAX];
char *redir_path;
char output_file[PATH_MAX];
char *output_path;
char cwd[PATH_MAX - MAXLEN];

/* lists/arrays */
Bind *binds_head, *binds_tail;
Chord *chords_ptr, *chords_head;
Keys *keys_head, *keys_tail;
Key *key_ptr, *key_head;
FreeList *free_head, *free_tail;

/* misc */
char opt_flag;
int redir_fd;
int keys_count;
unsigned int delim_len;
unsigned long Flags;

/* user globals */
char *Fonts[MAXLEN];
char *Colors[SchemeLast][3];
size_t fonts_len = LENGTH(fonts);
size_t keys_len  = LENGTH(keys);

/* non extern */
static char *press_keys;

static void
usage(void)
{
    fputs("Usage: wkx [-h|-v|-D|-w|-W|-b BINDS_FILE|-c CONFIG_FILE|-k KEYS_FILE|-o OUTPUT_FILE|-p KEY(s)|-r REDIR_FILE]\n"
          "\n"
          "wkx - Which-key via X.\n"
          "\n"
          "Options:\n"
          "  -h, --help                         Show this help message and exit.\n"
          "  -v, --version                      Print version and exit.\n"
          "  -d, --daemon                       Run wkx as a daemon.\n"
          "  -D, --debug                        Print debugging messages.\n"
          "  -w, --write-binds                  Write BINDS_FILE to binds.h.\n"
          "  -W, --write-keys                   Write KEYS_FILE to keys.h.\n"
          "  -b, --binds-file  BINDS_FILE       Read the binds from the given file.\n"
          "  -c, --conf-file   CONFIG_FILE      Read the main configuration from the given file.\n"
          "  -k, --keys-file   KEYS_FILE        Read the keys from the given file.\n"
          "  -o, --output      OUTPUT_FILE      Write parsed BINDS_FILE or KEYS_FILE to OUTPUT_FILE.\n"
          "  -p, --press-keys  KEY(s)           Press the entered KEY(s) before displaying.\n"
          "  -r, --redir-file  REDIR_FILE       Redirect commands output to the given file.\n", stderr);
    exit(EXIT_FAILURE);
}

int
main(int argc, char *argv[])
{
    char opt;
    define_globals();
    parse_conf_file();

    while (1) {
        static struct option long_opts[] = {
            /*                      no args                    */
            { "help",               no_argument,        0, 'h' },
            { "version",            no_argument,        0, 'v' },
            { "daemon",             no_argument,        0, 'd' },
            { "debug",              no_argument,        0, 'D' },
            { "write-binds",        no_argument,        0, 'w' },
            { "write-keys",         no_argument,        0, 'W' },
            /*                   required args                 */
            { "binds-file",         required_argument,  0, 'b' },
            { "conf-file",          required_argument,  0, 'c' },
            { "keys-file",          required_argument,  0, 'k' },
            { "output",             required_argument,  0, 'o' },
            { "press-keys",         required_argument,  0, 'p' },
            { "redir-file",         required_argument,  0, 'r' },
            { 0 }
        };
        int opt_ind = 0;
        opt = getopt_long(argc, argv, "hvdDwWb:c:k:o:p:r:",
                          long_opts, &opt_ind);
        if (opt == -1)
            break;

        switch (opt) {
        /* No args */
        case 'h':
            usage();
            break;
        case 'v':
            puts(VERSION);
            exit(EXIT_SUCCESS);
            break;
        case 'w':
            parse_binds = 1;
            opt_flag = opt;
            break;
        case 'd':
            opt_flag = opt;
            break;
        case 'W':
            parse_keys = 1;
            opt_flag = opt;
            break;
        case 'D':
            Flags |= DEBUG_FLAG;
            break;
        /* Requires arg */
        case 'b':
            binds_path  = optarg;
            parse_binds = 1;
            break;
        case 'c':
            config_path = optarg;
            parse_conf = 1;
            break;
        case 'k':
            keys_path  = optarg;
            parse_keys = 1;
            break;
        case 'o':
            output_path = optarg;
            break;
        case 'p':
            press_keys = optarg;
            opt_flag = opt;
            break;
        case 'r':
            redir_path = optarg;
            break;
        case '?':
            exit(EXIT_FAILURE);
            break;
        default:
            usage();
            break;
        }
    }

    if (opt_flag == 'd') {
        if (daemon_redir != NULL || redir_path != NULL)
            define_redir_file(daemon_redir, redir_path, redir_file, 0);
        parse_key_file();
        start_daemon();
        cleanup();
        return 0;
    } else if (opt_flag == 'W') {
        parse_key_file();
        define_output_file();
        if (write_keys_file(output_file) < 0) {
            cleanup();
            err("ERROR: could not open output file: '%s'",
                output_file);
        }
        cleanup();
        return 0;
    }
    if (def_redir != NULL || redir_path != NULL)
        define_redir_file(def_redir, redir_path, redir_file, 1);
    parse_binds_file();
    return opts_handler();
}

int
opts_handler(void)
{
    PUTS("Function: opts_handler");

    if (opt_flag == 'p') {
        int ret;
        ret = press_key(press_keys);
        if (!ret) {
            cleanup();
            return 0;
        } else if (ret < 0) {
            warn("WARNING: Key(s) not found '%s'\n", press_keys);
        }
    }

    if (opt_flag == 'w') {
        define_output_file();
        if (write_binds_file(output_file) < 0) {
            err("ERROR: could not open output file: '%s'",
                output_file);
        }
    } else {
        xwin(chords_ptr);
        return 0;
    }

    cleanup();

    return 0;
}

void
cleanup(void)
{
    FreeList *f = free_head;
    while (f != NULL) {
        FreeList *next = f->next;
        if (f->item != NULL && f->fp != NULL)
            f->fp(f->item);
        free(f);
        f = next;
    }
    if (redir_fd != -1)
        close(redir_fd);
}

void
early_free_binds(Bind *b)
{
    if (binds_head == NULL)
        return;
    while (b != NULL) {
        Bind *next = b->next;
        free(b->desc);
        free(b->cmd);
        free(b->key);
        free(b);
        b = next;
    }
    binds_head = binds_tail = NULL;
}

void
free_binds(Bind *b)
{
    while (b != NULL) {
        Bind *next = b->next;
        free(b);
        b = next;
    }
    binds_head = binds_tail = NULL;
}

void
free_chords(Chord *curr)
{
    if (curr == (Chord*)chords)
        return;
    Chord *head = curr;
    while (curr != NULL && curr->key != NULL) {
        free(curr->key);
        free(curr->cmd);
        free(curr->desc);
        if (curr->chords != NULL)
            free_chords(curr->chords);
        ++curr;
    }
    free(head);
}

void
free_cmd(char **cmd)
{
    char **head = cmd;
    while (*cmd != NULL)
        free(*cmd++);
    free(head);
}

void
free_fonts(FontList *f)
{
    while (f != NULL) {
        FontList *next = f->next;
        free(f->font);
        free(f);
        f = next;
    }
    font_list_head = NULL;
}

void
free_keys_list(Keys *k)
{
    while (k != NULL) {
        Keys *next = k->next;
        if (!(Flags & KARAY_FLAG))
            free(k->cmd);
        free(k);
        k = next;
    }
    keys_head = keys_tail = NULL;
}

void
free_keys_array(Key *k)
{
    while (k->cmd != NULL) {
        free(k->cmd);
        ++k;
    }
    free(key_ptr);
    key_ptr = NULL;
}

void
define_globals(void)
{
    /* Files/paths */
    binds_file[0]   = '\0';
    binds_path      = NULL;
    config_file[0]  = '\0';
    config_path     = NULL;
    keys_file[0]    = '\0';
    keys_path       = NULL;
    redir_file[0]   = '\0';
    redir_path      = NULL;
    output_file[0]  = '\0';
    output_path     = NULL;
    cwd[0]          = '\0';

    /* lists/arrays */
    binds_head = binds_tail  = NULL;
    chords_ptr = chords_head = NULL;
    keys_head  = keys_tail   = NULL;
    key_ptr    = key_head    = NULL;
    free_head  = free_tail   = make_free_list();
    if (free_head == NULL)
        err("ERROR: Could not allocate memory for free list\n");

    /* misc */
    opt_flag    = '\0';
    redir_fd    = -1;
    keys_count  = 0;
    delim_len   = 0;
    Flags       = 0;
    press_keys  = NULL;
}
