/* See LICENSE file for copyright and license details. */
#ifndef WKX_HELPERS_H_
#define WKX_HELPERS_H_
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

#include "types.h"

#define MAXLEN 256
#define ESC_TEST(c)         (c == '\'' || c == '\"' || c == '\\')
#define ISSPACE(s)          (isspace((unsigned char)*s))
#define LENGTH(arr)         (sizeof((arr)) / sizeof((arr)[0]))
#define LSTRIP(s)           while (ISSPACE(s)) \
                                ++s
#define PRINTF(x,...)       if (Flags & DEBUG_FLAG) \
                                printf(x, __VA_ARGS__)
#define PUTS(x)             if (Flags & DEBUG_FLAG) \
                                puts(x)
#define RSTRIP(s,o)         while (s > o && ISSPACE(s)) \
                                --s; \
                            ++s

void warn(char *fmt, ...);
void err(char *fmt, ...);
int press_key(char *key);
Chord *find_key(const char *buf, Chord *curr);
void daemon_spawn(char *cmd, Display *dpy);
void spawn(char *cmd);
void define_redir_file(const char def[], const char *path, char *file, const int trunc_flag);
void define_tmp_file(const char *file, char *bucket);

#endif /* WKX_HELPERS_H_ */
